<?php

require "Seguranca.php";

class Dashboard extends Seguranca{

    
    public function index(){
        if($this->usuario->id_perfil != 3){
            $this->load->model("UsuarioModel");
            $this->load->model("UsuarioEventoModel");
            $this->load->model("CargoModel");
            $usuarios = $this->UsuarioModel->listar();
            
            // Manipulação de cargos
            $nomes_cargos = array();
            $cargo = NULL;

            foreach ($usuarios as $usuario) {
                if($usuario->id_cargo != NULL){
                    $cargo = $this->UsuarioModel->encontraCargoDoUsuario($usuario->id_cargo);
                }else{
                    $cargo = "-";
                }
                $nomes_cargos[$usuario->id]=$cargo;
            }


            $usuarios = $this->carregarStatusParaLista($usuarios);
            $dados = array(
                'usuarios'=>$usuarios,
                'cargos'=>$nomes_cargos,

            );

            $conteudo = $this->load->view("Dashboard/Usuarios", $dados, true);
            $this->session->unset_userdata("mensagem");
        }
        else{
            $conteudo = "Não é admin!";
        }
        $this->carregarPagina($conteudo);
    }
     
    public function recuperarPorId(){
        if($_POST){
            $id = $_POST["id"];
        }
    }

    public function carregarStatusParaLista($usuarios){
        $id_evento = $this->session->evento->id;
        $this->load->model("UsuarioEventoModel");
        $usuarios = $this->UsuarioEventoModel->carregarStatusValidos($id_evento, $usuarios);
        return $usuarios;
    }

}
