<?php

require "Seguranca.php";

class Email extends Seguranca{

    public function index(){}

    public function enviar(){
        $this->load->library("Notificacao");

        $nome = "Rafael Ferreira dos Santos";
        $email_pessoal = "raafael.saantos@gmail.com";

        $texto = "<h3>Prezado(a) $nome,</h3>";
        $texto .= "<p>Os seus dados foram atualizados com sucesso!.</p>";
        $texto .= "<p>Agradecemos por fornecer o seu e-mail pessoal.</p>";
        $texto .= "<p>A Coordenação-Geral de Serviços de Tecnologia da Informação agradece e tenha um ótimo dia.</p>";

        $this->notificacao->enviar($email_pessoal,"Cadastro atualizado com sucesso",$texto);
    }

}

?>