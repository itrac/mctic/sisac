<?php
require "Seguranca.php";

class Evento extends Seguranca{
	public function index(){
		$this->load->model("EventoModel");
		$this->load->model("UsuarioEventoModel");
		$eventos = $this->EventoModel->listar();

		$eventos = $this->formataTodasDatas($eventos);

		// Obtém a quantidade de usuários atualizados
		foreach($eventos as $evento){
			$evento->numeroDeAtualizados = $this->UsuarioEventoModel->usuariosAtualizadosNoEvento($evento->id);
		}

		$dados = array(
			'usuario'=>$this->usuario,
			'eventos'=>$eventos
		);
		$conteudo = $this->load->view("Evento/Mostrar", $dados, true);
		$this->carregarPagina($conteudo);
	}

	private function formataTodasDatas($eventos){
		foreach($eventos as $evento){
			$evento->data_inicio = $this->formataData($evento->data_inicio);
			$evento->data_fim = $this->formataData($evento->data_fim);
		}
		return $eventos;
	}

	private function formataData($data){
		$ano = substr($data,0,4);
		$mes = substr($data,5,2);
		$dia = substr($data,8,2);

		$dataFormatada = $dia . "/" . $mes . "/" . $ano;
		return $dataFormatada;
	}

	public function criarEvento(){
			$nomeEvento=$this->input->post('nomeEvento');
			$dataInicio=$this->input->post('dataInicio');
			$dataFim=$this->input->post('dataFim');

			$dataInicioFormatada = $this->formataDataParaBanco($dataInicio);
			$dataFimFormatada = $this->formataDataParaBanco($dataFim);

			$dados = array(
				'evento'=>trim($nomeEvento),
				'data_inicio'=>$dataInicioFormatada,
				'data_fim'=>$dataFimFormatada
			);

			$this->load->model("EventoModel");
			$resultado = $this->EventoModel->inserir($dados);

			redirect('/evento');
	}

	public function alterarEvento(){
		$nomeEvento=$this->input->post('nomeEvento');
		$dataInicio=$this->input->post('dataInicio');
		$dataFim=$this->input->post('dataFim');
		$idEvento=$this->input->post('idEvento');

		$dataInicioFormatada = $this->formataDataParaBanco($dataInicio);
		$dataFimFormatada = $this->formataDataParaBanco($dataFim);

		$dados = array(
			'evento'=>trim($nomeEvento),
			'data_inicio'=>$dataInicioFormatada,
			'data_fim'=>$dataFimFormatada
		);

		$this->load->model("EventoModel");
		$resultado = $this->EventoModel->alterar($idEvento,$dados);

		redirect('/evento');
	}

	public function formataDataParaBanco($data){
		list($dia, $mes, $ano) = explode("/", $data);
		$dataFormatada = $ano . "-" . $mes . "-" . $dia;
		return $dataFormatada;
	}
}
?>
