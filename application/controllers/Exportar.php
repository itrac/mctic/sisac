<?php

require "Seguranca.php";

class Exportar extends Seguranca{
	function index(){
		$this->load->model('ImportacaoExportacaoModel');
		$datas = $this->ImportacaoExportacaoModel->listar();
		@$dataFormatada = $this->formataData($datas);

		$dados = array(
			'dataUltimaExportacao'=>$dataFormatada
		);

		$conteudo = $this->load->view("Exportar/Mostrar", $dados, true);
		$this->carregarPagina($conteudo);
	}

	function exportarParaOAd(){
		$this->load->library('ActiveDirectory');
		$this->load->model('UsuarioModel');
		$this->load->model('UsuarioEventoModel');
		$this->load->model('ImportacaoExportacaoModel');

		$usuario = $this->UsuarioModel->recuperarPorId($this->session->usuario->id);
		$sucesso = $this->activedirectory->fazerBind();
		$statusDaEscrita = "";
		if(TRUE === $sucesso){
			$usuarios = $this->UsuarioModel->listar();
			$ldap_connection = $this->activedirectory->retornarLdapConnection();
			$statusDaEscrita = $this->substituirAtributos($ldap_connection, $usuarios);
			$this->activedirectory->fazerUnbind();
		}else{
			$statusDaEscrita = "<pre>Erro ao realizar exportação <i class='fa fa-exclamation-circle' style='color:red'></i><br/>Confira as configurações de conexão com o AD (Active Directory).</pre>";
		}
	
		$this->load->model('ImportacaoExportacaoModel');
		$dataUltimaExportacao = $this->ImportacaoExportacaoModel->listar();
		@$dataUltimaExportacaoFormatada = $this->formataData($dataUltimaExportacao);
		
		$dados = array(
			'mensagem'=>$statusDaEscrita,
			'dataUltimaExportacao'=>$dataUltimaExportacaoFormatada
		);

		$conteudo = $this->load->view("Exportar/Mostrar", $dados, true);
		$this->carregarPagina($conteudo);
	}

	function substituirAtributos($ldap_connection, $users)
	{
		$erros = [];
		foreach($users as $user) {
			$firstName = strstr($user->nome, ' ', true);
			$lastName = trim(str_replace($firstName, "", $user->nome));

			if($user->id_empresa != NULL){
				$userCompany = $this->UsuarioModel->encontraEmpresaDeUsuario($user->id_empresa);
			}else{
				$userCompany = "";
			}
			if($user->id_bloco != NULL){
				$userBuilding = $this->UsuarioModel->encontraBlocoDoUsuario($user->id_bloco);
			}else{
				$userBuilding  = "";
			}
			if($user->id_cargo != NULL){
				$userCargo = $this->UsuarioModel->encontraCargoDoUsuario($user->id_cargo);
			}else{
				$userCargo  = "";
			}
			if($user->id_lotacao != NULL){
				$userCoordination = $this->UsuarioModel->encontrarCoordenacaoDoUsuario($user->id_lotacao);
			}else{
				$userCoordination  = "";
			}
			if($user->id_ocupacao != NULL){
				$userOccupation = $this->UsuarioModel->encontrarOcupacaoDoUsuario($user->id_ocupacao);
			}else{
				$userOccupation = "";
			}
			if($user->id_setor != NULL){
				$userSetor = $this->UsuarioModel->encontrarSetorDoUsuario($user->id_setor);
			}else{
				$userSetor = "";
			}
			if($user->outra_ocupacao != NULL && !empty($user->outra_ocupacao) ){
				$outra_ocupacao = $user->outra_ocupacao;
			}else{
				$outra_ocupacao = "";
			}

			$attributes = $this->insereDadosParaAtualizar($user, $firstName, $lastName, $userCompany, $userBuilding, $userCargo, $userCoordination, $userOccupation, $userSetor, $outra_ocupacao);
			$attributes = $this->removeCamposVazios($attributes);

			$result = null;
                        $this->load->model('UsuarioEventoModel');
			$atualizado = $this->UsuarioEventoModel->recuperarStatus($user->id, $this->session->evento->id);

			if($user->id_ad != NULL){
								
				if($user->valido && $atualizado){
					$result = ldap_mod_replace($ldap_connection, $user->id_ad , $attributes);
				}
			}

			if($atualizado && $result == false && $user->nome_usuario != "admin" && $user->nome_usuario != "usuario.db"){
				$erros[] = (string)$user->id_ad;
			}
		}

		$message = null;	
		$message = $this->preparaMensagemDeStatus($erros);

		return $message;
	}
	
	private function preparaMensagemDeStatus($erros=null){
		if(empty($erros)){
			$message = "<pre>Exportação realizada com sucesso <i class='fa fa-check' style='color:green'></i><br/>Os dados foram inseridos no AD sem erros.</pre>";
			if(empty($this->ImportacaoExportacaoModel->listar())){
				$this->criaHoraDeExportacao();
			}else{
				$this->alteraHoraDeExportacao();
			}
		}else{
			$message = "Ocorreu problemas de inserção <i class='fa fa-exclamation-circle' style='color:red'></i>:<br>";
			foreach($erros as $erro){
				$message = $message . " Usuario não encontrado no AD: " . $erro . "<br>";
			}
		}
		return $message;
	}

	public function alteraHoraDeExportacao(){
		date_default_timezone_set('America/Sao_Paulo');
		$date = date('Y-m-d H:i:s');

		$dados = array(
			'data_ultima_exportacao'=>$date
		);

		$this->ImportacaoExportacaoModel->atualizaData($dados);
	}

	public function criaHoraDeExportacao(){
		date_default_timezone_set('America/Sao_Paulo');
		$date = date('Y-m-d H:i:s');

		$dados = array(
			'data_ultima_exportacao'=>$date
		);

		$this->ImportacaoExportacaoModel->inserir($dados);
	}

	public function insereDadosParaAtualizar($user, $firstName, $lastName, $userCompany, $userBuilding, $userCargo, $userCoordination, $userOccupation, $userSetor, $outra_ocupacao){

		$attributes['sAMAccountName'] = (string)$user->nome_usuario;
		$attributes['givenName'] = (string) $firstName;
		$attributes['sn'] = (string) $lastName;
		$attributes['mail'] = (string)$user->email_institucional;
		$attributes['wWWHomePage'] = (string)$user->email_pessoal;
		$attributes['postOfficeBox'] = (string)$user->cpf;
		$attributes['telephoneNumber'] = (string)$user->telefone;
		$attributes['mobile'] = (string)$user->telefone_pessoal;
		$attributes['company'] = (string) $userCompany;
		$attributes['l'] = (string)$user->sala;
		$attributes['streetAddress'] = (string) $userBuilding;
		$attributes['physicalDeliveryOfficeName'] = (string) $userCoordination;
		$attributes['title'] = (string) $userOccupation;
		$attributes['pager'] = (string) $userCargo;
		$attributes['department'] = (string) $userSetor;
		$attributes['postalCode'] = (string)$user->andar;
		$attributes['st'] = (string)$user->estacao;
		$attributes['description'] = (string) $outra_ocupacao;

		return $attributes;
	}

	public function removeCamposVazios($attributes){
		foreach($attributes as $key => $item){
			if(trim($item) == ""){
				unset($attributes[$key]);
			}
		}

		return $attributes;
	}

	public function formataData($datas){
		$dataExportacao = $datas[0]->data_ultima_exportacao;

		if($dataExportacao){
			$date = date_create($dataExportacao);
		}
		else
			return false;

		$dataFormatada = date_format($date, 'd/m/Y H:i:s');
		return $dataFormatada;
	}
}
?>
