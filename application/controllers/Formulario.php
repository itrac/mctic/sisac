<?php

require "Seguranca.php";

class Formulario extends Seguranca{

    //Renderiza a primeira página
    public function index(){
    }

    public function criar(){
    }

    public function recuperarEmpresas(){
        $this->load->model('EmpresaModel');

        if (isset($_GET['term'])){
            $q = strtolower($_GET['term']);
            $this->EmpresaModel->recuperarEmpresa($q);
        }
    }

    public function recuperarIdEmpresa($nome){
        $this->load->model('EmpresaModel');
        $id = $this->EmpresaModel->obterIdEmpresa($nome);

        //Validação para empresa Nula
        if($nome == NULL && $id == 1){
            return NULL;
        }

        if($id){
            return $id;
        }
        else{
            $empresa = array(
                'empresa'=>$nome
            );
            return $this->EmpresaModel->inserir($empresa);
        }
    }

    public function removerCaracteresEspeciais($str) {
        $str = preg_replace('/[áàãâä]/ui', 'a', $str);
        $str = preg_replace('/[éèêë]/ui', 'e', $str);
        $str = preg_replace('/[íìîï]/ui', 'i', $str);
        $str = preg_replace('/[óòõôö]/ui', 'o', $str);
        $str = preg_replace('/[úùûü]/ui', 'u', $str);
        $str = preg_replace('/[ç]/ui', 'c', $str);
        $str = preg_replace('/[,(),;:|!"#$%&\/=?´\'\[\]~^><ªº-]/', '', $str);
    return $str;
  }

    public function salvar(){
        if($post = $this->input->post()){
            $id = isset($_POST['id']) ? $_POST['id'] : 0;
            extract($post);

            $this->load->model("UsuarioModel");
            $this->load->model("UsuarioEventoModel");

            $usuario = $this->UsuarioModel->recuperarPorId($this->usuario->id);
            $retorno = array("sucesso"=>false, "texto"=>"");
            if($id_bloco != 3){
              $id_setor = 3;
            }

            $dados = array();

            if($this->validar($dados)){
                if(!$id){
                    $retorno["texto"] = "Erro ao realizar atualização cadastral!";
                }
                else{
                    $this->session->mensagem = "Atualização Cadastral realizada com sucesso!";
                    $this->load->library("Notificacao");

                    if($id_ocupacao == 2){ // servidor comissionado
                        $id_cargo = $id_servidor_comissionado;
                    } elseif($id_ocupacao == 3){ //comissionado
                        $id_cargo = $id_comissionado;
                    } else {

                    }
                    //Validação para Outra ocupação Nula
                    if(strcmp(" ", $this->removerCaracteresEspeciais($outra_ocupacao))==1){
                        $outra_ocupacao_temp == NULL;
                    } else {
                        $outra_ocupacao_temp = $this->removerCaracteresEspeciais($outra_ocupacao);
                    }

                    $dados = array(
                        'id_ocupacao'=>$id_ocupacao,
                        'id_cargo'=>$id_cargo,
                        'id_perfil'=>$id_perfil,
                        'id_bloco'=>$id_bloco,
                        'id_lotacao'=>$id_lotacao,
                        'nome'=>$this->removerCaracteresEspeciais($nome),
                        'telefone'=>$telefone,
                        'telefone_pessoal'=>$telefone_pessoal,
                        'email_institucional'=>$email_institucional,
                        'email_pessoal'=>$email_pessoal,
                        'cpf'=>$cpf,
                        'sala'=>$this->removerCaracteresEspeciais($sala),
                        'andar'=>$this->removerCaracteresEspeciais($andar),
                        'id_empresa'=>$this->recuperarIdEmpresa($id_empresa),
                        'outra_ocupacao'=>$outra_ocupacao_temp,
                        'id_setor'=>$id_setor,
                        'estacao'=>$estacao,
                    );

                    $this->atualizarUsuarioEvento($id);

                    $this->UsuarioModel->alterar($id, $dados);

                    $nome = $dados['nome'];
                    $email_pessoal = $dados['email_pessoal'];
                    $email_institucional = $dados['email_institucional'];

                    $agradecimentoCadastro = "<h3>Prezado(a) $nome</h3>";
                    $agradecimentoCadastro .= "<p>Seus dados foram atualizados com sucesso!.</p>";
                    $agradecimentoCadastro .= "<p>A Coordenação-Geral de Serviços de Tecnologia da Informação- CGTI agradece a sua colaboração à nossa Campanha de Atualização Cadastral de usuários de TIC.</p>";
                    $agradecimentoCadastro .= "<p>Tenha um ótimo dia!</p>";

                    //$this->notificacao->enviar($email_institucional,"Cadastro atualizado com sucesso.",$agradecimentoCadastro);
                    
                    $email_pessoal = trim($email_pessoal);
                    if(!empty($email_pessoal)){
                        $agradecimentoEmailPessoal = "<h3>Prezado(a) $nome</h3>";
                        $agradecimentoEmailPessoal .= "<p>Você indicou esse endereço de e-mail pessoal como contato alternativo ao seu email institucional $email_institucional .</p>";
                        $agradecimentoEmailPessoal .= "<p>A Coordenação-Geral de Serviços de Tecnologia da Informação - CGTI agradece a sua colaboração à nossa Campanha de Atualização Cadastral de usuários de TIC.</p>";
                        $agradecimentoEmailPessoal .= "<p>Tenha um ótimo dia!</p>";

                        //$this->notificacao->enviar($email_pessoal,"Cadastro atualizado com sucesso.",$agradecimentoEmailPessoal);
                    }

                }
            }
        }

        redirect("/usuario");
    }

    private function atualizarUsuarioEvento($id_usuario){
        $id_evento = $this->session->evento->id;
        $dataAtual = date("Y-m-d H:i:s");
        $dadosAtualizacao = array('id_evento'=> $id_evento, 'id_usuario'=> $id_usuario, 'data_atualizado'=>$dataAtual);

        if($this->UsuarioEventoModel->recuperarStatus($id_usuario, $id_evento )){
            $this->UsuarioEventoModel->atualizarAlteracoes($dadosAtualizacao);
        }else{
            $this->UsuarioEventoModel->inserir($dadosAtualizacao);
        }
    }

    private function validar($dados){
        return true;
    }

}

?>
