<?php

require "Seguranca.php";

class Importar extends Seguranca {

    function index(){
		$this->load->model("ImportacaoExportacaoModel");
		$datas = $this->ImportacaoExportacaoModel->listar();

		@$dataFormatada = $this->formataDataImportada($datas);

		$dados = array(
			'dataUltimaImportacao'=>$dataFormatada
		);

		$conteudo = $this->load->view("Importar/Mostrar", $dados, true);
		$this->carregarPagina($conteudo);
    }

	function importarUsuarioParaBD(){
		$this->load->library("ActiveDirectory");
    	$this->load->model("UsuarioModel");
		$this->load->model("EmpresaModel");
		$this->load->model("CargoModel");
		$this->load->model("BlocoModel");
    	$this->load->model("SetorModel");
		$this->load->model("LotacaoModel");
		$this->load->model("OcupacaoModel");
		$this->load->model("EventoModel");
		$this->load->model("UsuarioEventoModel");

		$usuariosDoSql = $this->UsuarioModel->listar();
		$usuarioDoSql = $this->invalidarTodosUsuarios($usuariosDoSql);

		$usuariosAD = $this->activedirectory->listarUsuarios();

		$usuariosDoSql = $this->validaUsuariosDoAd($usuariosDoSql, $usuariosAD);

		$this->alteraNoBancoUsuariosExcluidos($usuariosDoSql);
		$novosUsuarios = $this->encontraNovosUsuariosNoAD($usuariosAD, $usuariosDoSql);
		$this->insereNoBancoNovosusuarios($novosUsuarios);
		$this->importarUsuariosNaoAtualizados($usuariosDoSql, $usuariosAD);
		$arrayAtual = array_merge($usuariosDoSql, $novosUsuarios);

		$quantidadeDeUsuarios = count($usuariosAD);

		$dados = array(
			'quantidade_usuarios'=>$quantidadeDeUsuarios
		);

		$this->EventoModel->atualizaQuantidadeUsuarios($this->session->evento->id,$dados);
		$this->load->model('UsuarioEventoModel');

        foreach($usuariosAD as $usuarioAD){
            $usuario = $this->UsuarioModel->recuperarPorNomeUsuario($usuarioAD['nome_usuario']);
            if(!$usuario){
				$usuarioAD['id_perfil'] = 3;
				$usuarioAD['valido'] = 1;
				$usuarioAD['data_importado'] = date("Y-m-d H:i:s");
				$usuarioAD['id_cargo'] = $this->CargoModel->obterIdCargo($usuarioAD['id_cargo']);
				$usuarioAD['id_empresa'] = $this->EmpresaModel->encontrarIdEmpresa($usuarioAD['id_empresa']);
				$usuarioAD['id_bloco'] = $this->BlocoModel->obterIdBloco($usuarioAD['id_bloco']);
				$usuarioAD['id_lotacao'] = $this->LotacaoModel->obterIdLotacao($usuarioAD['id_lotacao']);
				$usuarioAD['id_ocupacao'] = $this->OcupacaoModel->obterIdOcupacao($usuarioAD['id_ocupacao']);
      			$usuarioAD['id_setor'] = $this->SetorModel->obterIdSetor($usuarioAD['id_setor']);
                $this->UsuarioModel->inserir($usuarioAD);
            }else{ // caindo aqui tem o usuário no banco
            	$atualizado = $this->UsuarioEventoModel->recuperarStatus($usuario->id, $this->session->evento->id);
            	if(!$atualizado){
	                $usuario->nome = $usuarioAD['nome'];
	                $usuario->telefone = $usuarioAD['telefone'];
	                $usuario->email_institucional = $usuarioAD['email_institucional'];
	                $usuario->email_pessoal = $usuarioAD['email_pessoal'];
	                $usuario->cpf = $usuarioAD['cpf'];
	                $usuario->nome_usuario = $usuarioAD['nome_usuario'];
	                $usuario->sala = $usuarioAD['sala'];
	                $usuario->andar = $usuarioAD['andar'];
					$usuario->id_ad = $usuarioAD['id_ad'];
					$usuario->id_cargo = $this->CargoModel->obterIdCargo($usuarioAD['id_cargo']);
					$usuario->id_empresa = $this->EmpresaModel->encontrarIdEmpresa($usuarioAD['id_empresa']);
					$usuario->id_bloco = $this->BlocoModel->obterIdBloco($usuarioAD['id_bloco']);
					$usuario->id_lotacao = $this->LotacaoModel->obterIdLotacao($usuarioAD['id_lotacao']);
					$usuario->id_ocupacao = $this->OcupacaoModel->obterIdOcupacao($usuarioAD['id_ocupacao']);
	      			$usuario->id_setor = $this->SetorModel->obterIdSetor($usuarioAD['id_setor']);
	      			$usuario->estacao = $usuarioAD['estacao'];
					$usuario->outra_ocupacao = $usuarioAD['outra_ocupacao'];
	                $this->UsuarioModel->alterar($usuario->id, $usuario);	
            	}else{
            		// já foi atualizado no evento, não substituir seus dados
            	}
            }
		}

		if($usuariosAD){
			$message = "<pre>Importação realizada com sucesso <i class='fa fa-check' style='color:green'></i></pre>";
			$dataUltimaImportacao = $this->atualizaHoraUltimaImportacao();
		}else{
			$message = "<pre>Erro ao realizar importação <i class='fa fa-exclamation-circle' style='color:red'></i><br/>Confira as configurações de conexão com o AD (Active Directory).</pre>";
			$this->load->model("ImportacaoExportacaoModel");
			$datas = $this->ImportacaoExportacaoModel->listar();
			@$dataFormatada = $this->formataDataImportada($datas);
			$dataUltimaImportacao = $dataFormatada;
		}

		
		$dados = array(
			'mensagem'=>$message,
			'dataUltimaExportacao'=>$dataUltimaImportacao
		);

		$conteudo = $this->load->view("Importar/Mostrar", $dados, true);
		$this->carregarPagina($conteudo);
	}

	private function importarUsuariosNaoAtualizados($usuariosDoSql, $usuariosAD){
		foreach($usuariosDoSql as $usuario){
			$atualizado = $this->UsuarioEventoModel->recuperarStatus($usuario->id, $this->session->evento->id);
			if(!$atualizado && $usuario->valido){
				$usuarioNaoAlterado = $this->obtemUsuarioDoAd($usuario, $usuariosAD);
				$usuarioNaoAlterado['id'] = $usuario->id;
				$usuarioNaoAlterado = $this->insereValoresDefaultEmNovoUsuario($usuarioNaoAlterado);
				$this->UsuarioModel->alterar($usuarioNaoAlterado['id'], $usuarioNaoAlterado);
			}
		}
	}

	private function insereValoresDefaultEmNovoUsuario($usuario){
		$usuario['id_perfil'] = 3;
		$usuario['valido'] = 1;
		$usuario['data_importado'] = date("Y-m-d H:i:s");
		$usuario['id_bloco'] = $this->BlocoModel->obterIdBloco($usuario['id_bloco']);
		$usuario['id_lotacao'] = $this->LotacaoModel->obterIdLotacao($usuario['id_lotacao']);
		$usuario['id_empresa'] = $this->EmpresaModel->encontrarIdEmpresa($usuario['id_empresa']);
		$usuario['id_cargo'] = $this->CargoModel->obterIdCargo($usuario['id_cargo']);
		$usuario['id_ocupacao'] = $this->OcupacaoModel->obterIdOcupacao($usuario['id_ocupacao']);
    $usuario['id_setor'] = $this->SetorModel->obterIdSetor($usuario['id_setor']);

		return $usuario;
	}

	private function insereNoBancoNovosusuarios($novosUsuarios){
		foreach($novosUsuarios as $usuario){
			$usuario['valido'] = 1;
			$usuario = $this->insereValoresDefaultEmNovoUsuario($usuario);
			$this->UsuarioModel->inserir($usuario);
		}
	}

	private function encontraNovosUsuariosNoAD($usuariosDoAD, $usuariosDoSql){
		$usuarioNovo = false;
		$arrayAtual = array();
		foreach($usuariosDoAD as $usuarioDoAD){
			$usuarioNovo = $this->encontraUsuarioNoBanco($usuarioDoAD, $usuariosDoSql);
			if($usuarioNovo){
				array_push($arrayAtual,$usuarioDoAD);
			}
		}
		return $arrayAtual;
	}

	private function encontraUsuarioNoBanco($usuarioDoAd, $usuariosDoSql){
		$usuarioNovo = false;
		$encontrou = false;
		foreach($usuariosDoSql as $usuarioDoSql){

			if(strcmp($usuarioDoSql->id_ad, $usuarioDoAd['id_ad']) == 0){
				$encontrou = true;
			}
		}
		if($encontrou == false){
			$usuarioNovo = true;
		}
		return $usuarioNovo;
	}

	private function alteraNoBancoUsuariosExcluidos($usuariosDoSql){
		foreach($usuariosDoSql as $usuarioDoSql){
			if($usuarioDoSql->valido == 0){
				if($usuarioDoSql->nome_usuario != "admin" && $usuarioDoSql->nome_usuario != "usuario.db"){
					$dados = array(
						'valido' => $usuarioDoSql->valido
					);
					$this->UsuarioModel->alterar($usuarioDoSql->id, $dados);
				}
			}
		}
	}

	private function invalidarTodosUsuarios($usuariosDoSql){
		foreach($usuariosDoSql as $userSql){
			$userSql->valido = 0;
		}
		return $usuariosDoSql;
	}

	private function validaUsuariosDoAd($usuariosDoSql, $usuariosDoAD){
		$encontrouNoAd = false;
		foreach($usuariosDoSql as $usuarioDoSql){
			$encontrouNoAd = $this->procuraUsuario($usuarioDoSql, $usuariosDoAD);
			if($encontrouNoAd){
				$usuarioDoSql->valido = 1;
			}
		}
		return $usuariosDoSql;
	}

	private function procuraUsuario($usuarioDoSql, $usuariosDoAD){
		$encontrouNoAd = false;
		foreach($usuariosDoAD as $usuarioDoAD){
			if( strcmp($usuarioDoAD['id_ad'] ,$usuarioDoSql->id_ad) == 0){
				$encontrouNoAd = true;
			}
		}
		return $encontrouNoAd;
	}

	private function obtemUsuarioDoAd($usuarioDoSql, $usuariosDoAD){
		$usuario = null;
		foreach($usuariosDoAD as $usuarioDoAD){
			if( strcmp($usuarioDoAD['id_ad'] ,$usuarioDoSql->id_ad) == 0){
				$usuario = $usuarioDoAD;
			}
		}
		return $usuario;
	}

	private function atualizaHoraUltimaImportacao(){
		$this->load->model("ImportacaoExportacaoModel");
		if(empty($this->ImportacaoExportacaoModel->listar())){
			$this->criaHoraDeImportacao();
		}else{
			$this->alteraHoraDeImportacao();
		}
		$horaImportacao = $this->ImportacaoExportacaoModel->listar()[0]->data_ultima_importacao;
		return $horaImportacao;
	}


	public function alteraHoraDeImportacao(){
		date_default_timezone_set('America/Sao_Paulo');
		$date = date('Y-m-d H:i:s');

		$dados = array(
			'data_ultima_importacao'=>$date
		);

		$this->ImportacaoExportacaoModel->atualizaData($dados);
	}

	public function criaHoraDeImportacao(){
		date_default_timezone_set('America/Sao_Paulo');
		$date = date('Y-m-d H:i:s');

		$dados = array(
			'data_ultima_importacao'=>$date
		);

		$this->ImportacaoExportacaoModel->inserir($dados);
	}

	public function formataDataImportada($datas){
		$dataImportacao = @$datas[0]->data_ultima_importacao;

		if($dataImportacao){
			$date = date_create($dataImportacao);
		}
		else
			return false;

		$dataFormatada = date_format($date, 'd/m/Y H:i:s');

		return $dataFormatada;
	}
}
