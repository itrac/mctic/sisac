<?php

require "Seguranca.php";

class Relatorio extends Seguranca {

    private $imagem;

    function __construct() {
        parent::__construct();

        $this->load->library('m_pdf');

        $this->m_pdf->pdf->setAutoTopMargin = true;

        $data = date("d/m/Y H:i:s");
        $this->m_pdf->pdf->SetFooter("Impresso em {$data} |Gerado por: {$this->usuario->nome}|Página {PAGENO} de {nbpg}");
    }

    function lotacao() {
        if($this->usuario->id_perfil != 3){
            $this->load->model("UsuarioModel");
            $this->load->model("UsuarioEventoModel");
            $this->load->model("CargoModel");
            $this->load->model("LotacaoModel");
            $this->load->model("BaseModel");

            $usuarios = $this->UsuarioModel->listar();
            $lotacao = $this->LotacaoModel->listar();
            $id_cargos = $this->CargoModel->listar();

            // Manipulação de cargos
            $nomes_cargos = array();
            $cargo = NULL;

            foreach ($usuarios as $usuario) {
                if($usuario->id_cargo != NULL){
                    $cargo = $this->UsuarioModel->encontraCargoDoUsuario($usuario->id_cargo);
                }else{
                    $cargo = "-";
                }
                $nomes_cargos[$usuario->id]=$cargo;
            }
           
            $dados = array(
                'usuarios'=>$usuarios,
                'lotacoes'=>$lotacao,
                'cargos'=>$nomes_cargos
            );

            $html = $this->load->view("Usuario/RelatorioLotacao", $dados, true);
        }

        $this->m_pdf->pdf->SetHeader("||Relatório Geral de usuários por Lotação ");
        $this->m_pdf->pdf->WriteHTML($html);
        $this->m_pdf->pdf->Output('Relatorio-Lotacao.pdf', 'I');
    }
    
    function eventosAntigos() {
        if ( isset($_GET['id']) ) {
            $this->load->model("UsuarioModel");
            $this->load->model("EventoModel");
            $this->load->model("UsuarioEventoModel");

            $usuarios = $this->UsuarioModel->listar();
            $eventoEncontrado = $this->EventoModel->recuperarPorId($_GET['id']);

            $this->UsuarioEventoModel->carregarStatus($_GET['id'], $usuarios);

            $usuarios = $this->filtrarUsuariosDoEvento($usuarios, $eventoEncontrado);

            // Manipulação de cargos
            $nomes_cargos = array();
            $cargo = NULL;

            foreach ($usuarios as $usuario) {
                if($usuario->id_cargo != NULL){
                    $cargo = $this->UsuarioModel->encontraCargoDoUsuario($usuario->id_cargo);
                }else{
                    $cargo = "-";
                }
                $nomes_cargos[$usuario->id]=$cargo;
            }
            $dados = array(
                'usuarios'=>$usuarios,
                'cargos'=>$nomes_cargos
            );

            $html = $this->load->view("Evento/RelatorioEventoAntigo", $dados, true);

            $nomeEvento = $eventoEncontrado->evento;
            $this->m_pdf->pdf->SetHeader("||Relatório do Evento: ".$nomeEvento);
        } else {
            $dados = array();
            $html = $this->load->view("Evento/RelatorioEventoAntigo", $dados, true);
            $this->m_pdf->pdf->SetHeader("||Evento sem usuários importados");
        }

          $this->m_pdf->pdf->WriteHTML($html);
          $this->m_pdf->pdf->Output('Relatorio-Evento.pdf', 'I');
    }


    private function filtrarUsuariosDoEvento($usuarios, $evento){
        $usuariosFiltrados = array();

        foreach($usuarios as $usuario){
            if ($usuario->nome_usuario != "admin" || $usuario->nome_usuario != "usuario.db"){
                if(strcmp($usuario->status,"Atualizado") == 0){
                    array_push($usuariosFiltrados,$usuario);
                }else{
                    if($this->verificaDatasRemocaoImportacao($usuario, $evento)){
                        array_push($usuariosFiltrados,$usuario);
                    }
                }
            }
        }

        return $usuariosFiltrados;
    }

    private function verificaDatasRemocaoImportacao($usuario, $evento){
        list($dataUsuarioImportado, $horaUsuarioImportado) = explode(" ", $usuario->data_importado);
        list($dataUsuarioRemovido, $horaUsuarioRemovido) = explode(" ", $usuario->data_removido);

        // Remove usuários de teste do Relatório

        if ($dataUsuarioRemovido == NULL){
            if ($dataUsuarioImportado <= $evento->data_fim){
                return true;
            }else{
                return false;
            }
        }else{
            if($dataUsuarioRemovido >= $evento->data_inicio && $dataUsuarioImportado <= $evento->data_fim){
                return true;
            }else{
                return false;
            }
        }
    }

}
