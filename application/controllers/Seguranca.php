<?php

class Seguranca extends CI_Controller{

    protected $usuario;
    protected $evento = NULL;

    public function __construct(){
        parent::__construct();
        $this->usuario = $this->session->usuario;
        $controle = strtolower($this->uri->segment(1));
        if(!$this->usuario && $controle!='seguranca'){
            redirect("/seguranca");
        }
    }

    public function index(){
        $mensagem = $this->session->mensagem;
        $this->session->unset_userdata("mensagem");
        $this->load->view("Seguranca/Login", array("mensagem"=>$mensagem));
    }

    public function login(){
        $this->load->library('ActiveDirectory');
        $this->load->model('UsuarioModel');
        if($post = $this->input->post()){
            extract($post);

            //Tenta logar no AD
            $sucesso = $this->activedirectory->autenticar($usuario,$senha);

            //Vai para o banco
            $usuario = $this->UsuarioModel->recuperarPorNomeUsuario($usuario);

            if($usuario){
                if($sucesso || (md5($senha) == $usuario->senha)) {
                    unset($usuario->senha);
                    $this->session->usuario = $usuario;
                    $this->session->evento = $this->carregarEventoAtual();
                    $this->UsuarioModel->atualizarUltimoAcesso($usuario->id);
                    if ($usuario->id_perfil == 3)
                        redirect("/usuario");
                    else
                        redirect("/dashboard");
                }
            }
            $this->session->mensagem = "Usuário/Senha inválido(s)";
            redirect("/seguranca");
        }
    }

    public function logout(){
        $this->session->sess_destroy();
        redirect("/seguranca");
    }

    protected function carregarPagina($conteudo){
        $this->load->view(recuperarPaginaInicial($this->usuario),array('conteudo'=>$conteudo,'usuario'=>$this->usuario));
    }

    protected function carregarEventoAtual(){
        $this->load->model("EventoModel");
        $eventos = $this->EventoModel->listar();
        $dataAtual = date("Y-m-d");
		
        foreach($eventos as $evento){
            if($dataAtual >= $evento->data_inicio && $dataAtual <= $evento->data_fim){
                $this->evento = $evento;
            }
        }
		if($this->evento == null){
			$this->evento->id = -1;
		}
        return $this->evento;
    }
}

?>
