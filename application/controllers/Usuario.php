<?php

require "Seguranca.php";

class Usuario extends Seguranca{

    public function index(){
        if($this->usuario->id_perfil == 3 && $this->session->evento->id != -1){
            $dados = $this->criarDados($this->usuario);
            $conteudo = $this->load->view("Usuario/Formulario", $dados, true);
        } elseif($this->usuario->id_perfil == 3 && $this->session->evento->id == -1){
            $this->session->mensagem = "Não há eventos de atualização ativos no momento!";
            $mensagem = $this->session->mensagem;
            redirect("/seguranca");

        } else {
            redirect("/dashboard?editado=ok");
        }
        $this->carregarPagina($conteudo);
    }

    public function editarUsuario(){
        $nomeUsuario = $_GET['nome_usuario'];
        $this->load->model("UsuarioModel");
        $usuario = $this->UsuarioModel->recuperarPorNomeUsuario($nomeUsuario);
        $dados = $this->criarDados($usuario);
        $conteudo = $this->load->view("Usuario/Formulario", $dados, true);
        $this->carregarPagina($conteudo);
    }

    private function criarDados($usuario){
        $this->load->model("OcupacaoModel");
        $this->load->model("LotacaoModel");
        $this->load->model("BlocoModel");
        $this->load->model("EmpresaModel");
        $this->load->model("CargoModel");
        $this->load->model("PerfilModel");
        $this->load->model("SetorModel");

        $empresa = $this->EmpresaModel->listar();
        $cargo = $this->CargoModel->listar();
        $ocupacao = $this->OcupacaoModel->listar();
        $bloco = $this->BlocoModel->listar();
        $empresa = $this->EmpresaModel->listar();
        $lotacao = $this->LotacaoModel->listar();
        $perfil = $this->PerfilModel->listar();
        $setor = $this->SetorModel->listar();

        $mensagem = $this->session->mensagem;
        $this->session->unset_userdata("mensagem");

        $nomeEmpresa = $this->EmpresaModel->recuperarPorId($usuario->id_empresa);

        $dados = array(
            'usuario'=>$usuario,
            "mensagem"=>$mensagem,
            'ocupacoes'=>$ocupacao,
            'blocos'=>$bloco,
            'empresas'=>$empresa,
            'cargos'=>$cargo,
            'lotacoes'=>$lotacao,
            'perfis'=>$perfil,
            'nomeEmpresa'=> $nomeEmpresa,
            'setores'=> $setor
        );
        return $dados;
    }
}

?>
