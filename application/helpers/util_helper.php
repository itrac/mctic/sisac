<?php

function formatarDataHoraBD($data){

    $arrayData = explode(" ",$data);
    $dataBD = implode("-",array_reverse(explode("/",$arrayData[0]))); 
    $novaData = $dataBD." ".$arrayData[1];

    return $novaData;
}

function recuperarPaginaInicial($usuario){
    return ($usuario->id_perfil != 3 ? "index_admin" : "index_usuario");
}