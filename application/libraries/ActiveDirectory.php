<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ActiveDirectory {
    // Credenciais de acesso do ministério
    private $ldap_password = 'Itrac17';
    private $ldap_username = 'Administrator';
    private $ldap_host = "172.16.5.103";
    private $ldap_domain = "@ITRAC.LOCAL";
    private $ldap_connection = null;

    public function __construct(){
        $this->ldap_connection = @ldap_connect($this->ldap_host);
        if(!$this->ldap_connection){
            throw new Exception("Erro ao conectar no AD!");
        }
        ldap_set_option($this->ldap_connection, LDAP_OPT_PROTOCOL_VERSION, 3) or die('Não foi possível conectar no AD.');
        ldap_set_option($this->ldap_connection, LDAP_OPT_REFERRALS, 0);
        ldap_set_option($this->ldap_connection, LDAP_OPT_NETWORK_TIMEOUT, 5);
    }

    public function autenticar($usuario, $senha){;
        $sucesso = @ldap_bind($this->ldap_connection, $usuario.$this->ldap_domain, $senha) === TRUE;
		
		ini_set('max_execution_time', 300);
        @ldap_unbind($this->ldap_connection);

        return $sucesso;
    }

    private function recuperarAtributo($item, $atributo){
        return trim(@$item[$atributo][0] ? @$item[$atributo][0] : "");
    }
	
	public function fazerBind(){
		ini_set('max_execution_time', 300);
		$sucesso = @ldap_bind($this->ldap_connection, $this->ldap_username.$this->ldap_domain, $this->ldap_password) === TRUE;
        return $sucesso;
	}
	
	public function fazerUnbind(){
		@ldap_unbind($this->ldap_connection);
	}

    public function listarUsuarios(){
        $retorno = array();

        if (TRUE === @ldap_bind($this->ldap_connection, $this->ldap_username.$this->ldap_domain, $this->ldap_password)){
			ini_set('max_execution_time', 300);
			
			$ldap_base_dn = 'OU=MCTIC,DC=ITRAC,DC=LOCAL';
            $search_filter = '(&(objectClass=user)(!(objectClass=computer)))';


            $attributes = array();
            $attributes[] = 'samaccountname'; //nome de usuário
            $attributes[] = 'givenname'; //primeiro nome
            $attributes[] = 'sn'; //sobrenome
            $attributes[] = 'mail'; //email institucional
            $attributes[] = 'wWWHomePage'; //email pessoal
            $attributes[] = 'postOfficeBox'; //CPF
            $attributes[] = 'telephoneNumber'; //Telefone
            $attributes[] = 'mobile'; //Telefone Pessoal
            $attributes[] = 'company'; //Empresa
            $attributes[] = 'l'; //Sala
            $attributes[] = 'streetAddress'; //Bloco
            $attributes[] = 'physicalDeliveryOfficeName'; //Coordenação
            $attributes[] = 'title'; //Ocupação
            $attributes[] = 'pager'; //Cargo?
            $attributes[] = 'postalCode'; //Andar
            $attributes[] = 'distinguishedName'; //id do AD para cada usuário
            $attributes[] = 'department'; 
            $attributes[] = 'st'; // Representa a estação de trabalho do usuário
	    $attributes[] = 'description'; // Representa a outra ocupacao do usuário
			$cookie = "";
			
			do {
				ldap_control_paged_result($this->ldap_connection, 1000, true, $cookie);
				$resultado = @ldap_search($this->ldap_connection, $ldap_base_dn, $search_filter, $attributes, null, 1000);
				$lista = @ldap_get_entries($this->ldap_connection, $resultado);
				
				if (FALSE !== $resultado){
					$lista = @ldap_get_entries($this->ldap_connection, $resultado);
					if($lista && is_array($lista)){
						for ($x=0; $x<$lista['count']; $x++){
							$nome_usuario = $this->recuperarAtributo($lista[$x], 'samaccountname');
							$nome = trim($this->recuperarAtributo($lista[$x], 'givenname') . ' ' . 
								$this->recuperarAtributo($lista[$x], 'sn'));
							$emailInstitucional = $this->recuperarAtributo($lista[$x], 'mail');
							$emailPessoal = $this->recuperarAtributo($lista[$x], 'wwwhomepage');
							$cpf = $this->recuperarAtributo($lista[$x], 'postofficebox');
							$telefone = $this->recuperarAtributo($lista[$x], 'telephonenumber');
							$telefone_pessoal = $this->recuperarAtributo($lista[$x], 'mobile');
							$empresa = $this->recuperarAtributo($lista[$x], 'company');
							$sala = $this->recuperarAtributo($lista[$x], 'l');
							$bloco = $this->recuperarAtributo($lista[$x], 'streetaddress');
							$lotacao = $this->recuperarAtributo($lista[$x], 'physicaldeliveryofficename');
							$ocupacao = $this->recuperarAtributo($lista[$x], 'title');
							$cargo = $this->recuperarAtributo($lista[$x], 'pager');
							$andar = $this->recuperarAtributo($lista[$x], 'postalcode');
							$dn = $this->recuperarAtributo($lista[$x], 'distinguishedname');
							$setor = $this->recuperarAtributo($lista[$x], 'department');
							$estacao = $this->recuperarAtributo($lista[$x], 'st');
							$outra_ocupacao = $this->recuperarAtributo($lista[$x], 'description');	

							if ($nome_usuario){
								$retorno[] = array(
									'nome' => $nome,
									'telefone' => strtolower($telefone),
									'telefone_pessoal' => strtolower($telefone_pessoal),
									'email_institucional' => strtolower($emailInstitucional),
									'email_pessoal' => strtolower($emailPessoal),
									'cpf' => $cpf,
									'nome_usuario' => strtolower($nome_usuario),
									'sala' => $sala,
									'andar' => $andar,
									'id_ad' => $dn,
									'id_empresa' => $empresa,
									'id_cargo' => $cargo,
									'id_bloco' => $bloco,
									'id_lotacao' => $lotacao,
									'id_ocupacao' => $ocupacao,
									'id_setor' => $setor,
									'estacao' => $estacao,
									'outra_ocupacao' => $outra_ocupacao
								);
							}
						}
					}
				}
				
				@ldap_control_paged_result_response($this->ldap_connection, $resultado, $cookie);
				
			} while($cookie !== null && $cookie != '');
	
            @ldap_unbind($this->ldap_connection);
        }

        return $retorno;
    }
	
	public function retornarLdapConnection(){
		return $this->ldap_connection;
	}
}
