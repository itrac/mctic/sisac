<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notificacao {

    private $nome = "Sistema de Atualização Cadastral - SisAC";
    private $email;
    private $config = array(
        'protocol'=>'smtp',
        'smtp_host'=>'ssl://correio.mctic.gov.br',
        'smtp_port'=>'465',
        'smtp_user'=>'sisac@mctic.gov.br',
        'smtp_pass'=>'Mctic@123',
        'charset'=>'utf-8',
        'mailtype'=>'html', // text ou html
        'validation'=>TRUE
    );
 
    public function __construct(){
        $CI =& get_instance();
        $CI->load->library('email');
        $this->email =& $CI->email;
        $this->email->initialize($this->config);
    }

    public function enviar($destinario, $assunto, $texto){
        $this->email->from($this->config['smtp_user'], $this->nome);
        $this->email->to($destinario); 

        $this->email->subject($assunto);
        $this->email->message($texto);

        if (!$this->email->send()) {
            show_error($this->email->print_debugger()); }
        else {
            echo 'E-mail enviado com sucesso!';
        }
    }

}