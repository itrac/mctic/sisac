<?php

class BaseModel extends CI_Model{
    protected $tabela;

    public function __contructor(){
        parent::__contructor();
        if(!$this->tabela)
            die("Informe a tabela!");
    }

    public function listar(){
        return $this->db->get($this->tabela)->result();
    }

    public function inserir($dados){
        $this->db->insert($this->tabela, $dados);
        return $this->db->insert_id();
    }

    public function alterar($id,$dados){
        $this->db->where('id',$id);
        $this->db->update($this->tabela, $dados);
        return $id;
    }

    public function excluir($id){
        $this->db->where('id',$id);
        $this->db->delete($this->tabela);
        return $id;
    }

    public function recuperarPorId($id){
        return $this->db->get_where($this->tabela,array("id"=>$id))->row();
    }
}