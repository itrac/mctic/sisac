<?php

require_once "BaseModel.php";

class BlocoModel extends BaseModel{
    protected $tabela = "bloco";
	
	function obterIdBloco($nome){
        $dados = $this->db->select('*')
        ->from($this->tabela)
        ->where("bloco", $nome)
        ->get()
        ->result();

        if(sizeof($dados) == 0)
            return null;
        else
            $id = $dados[0]->id;

        return $id;
    }
}