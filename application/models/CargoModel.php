<?php

require_once "BaseModel.php";

class CargoModel extends BaseModel{
    protected $tabela = "cargo";

    function obterIdCargo($nome){
        $dados = $this->db->select('*')
        ->from($this->tabela)
        ->where("cargo", $nome)
        ->get()
        ->result();

        if(sizeof($dados) == 0)
            return null;
        else
            $id = $dados[0]->id;

        return $id;
    }
}