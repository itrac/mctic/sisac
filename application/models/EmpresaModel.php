<?php

require_once "BaseModel.php";

class EmpresaModel extends BaseModel{
    protected $tabela = "empresa";

    function recuperarEmpresa($q){
        $this->db->select('*');
        $this->db->like('empresa', $q);
        $query = $this->db->get($this->tabela);
        if($query->num_rows() > 0){
          foreach ($query->result_array() as $row){
            $row_set[] = htmlentities(stripslashes($row['empresa']));
          }
          echo json_encode($row_set);
        }
    }

    function obterIdEmpresa($nome){
        $dados = $this->db->select('*')
        ->from($this->tabela)
        ->like("empresa", $nome)
        ->get()
        ->result();
        if(sizeof($dados) == 0)
            return NULL;
        else
            $id = $dados[0]->id;
        return $id;
    }
	
	function encontrarIdEmpresa($nome){
        $dados = $this->db->select('*')
        ->from($this->tabela)
        ->where("empresa", $nome)
        ->get()
        ->result();
        if(sizeof($dados) == 0)
            return null;
        else
            $id = $dados[0]->id;

        return $id;
    }
}