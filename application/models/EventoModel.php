<?php

require_once "BaseModel.php";

class EventoModel extends BaseModel{
    protected $tabela = "evento";

    public function atualizaQuantidadeUsuarios($id,$dados){
        $this->db->where('id',$id);
        $this->db->update($this->tabela, $dados);
        return $id;
    }

}
