<?php

require_once "BaseModel.php";

class ImportacaoExportacaoModel extends BaseModel{
    protected $tabela = "importacao_exportacao";
	
	public function atualizaData($dados){
        $this->db->update($this->tabela, $dados);
    }	
}

?>
