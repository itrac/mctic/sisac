<?php

require_once "BaseModel.php";

class LotacaoModel extends BaseModel{
    protected $tabela = "lotacao";

    function encontrarIdLotacao($nome){
        $lotacoes = $this->listar();

        foreach ($lotacoes as $lotacao) {
            $nomes = explode("/", $lotacao->lotacao);
            $ultimoNomeNoCaminho = end($nomes);
            if($ultimoNomeNoCaminho == $nome){
                return $lotacao->id;
            }
        }
        return null;
    }
	
	function obterIdLotacao($nome){
        $dados = $this->db->select('*')
        ->from($this->tabela)
        ->where("lotacao", $nome)
        ->get()
        ->result();

        if(sizeof($dados) == 0)
            return null;
        else
            $id = $dados[0]->id;

        return $id;
    }
}
