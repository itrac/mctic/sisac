<?php

require_once "BaseModel.php";

class OcupacaoModel extends BaseModel{
    protected $tabela = "ocupacao";
	
	function obterIdOcupacao($nome){
        $dados = $this->db->select('*')
        ->from($this->tabela)
        ->where("ocupacao", $nome)
        ->get()
        ->result();

        if(sizeof($dados) == 0)
            return null;
        else
            $id = $dados[0]->id;
        return $id;
    }
}