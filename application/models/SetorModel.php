<?php

require_once "BaseModel.php";

class SetorModel extends BaseModel{
    protected $tabela = "setor";

	function obterIdSetor($nome){
        $dados = $this->db->select('*')
        ->from($this->tabela)
        ->where("setor", $nome)
        ->get()
        ->result();

        if(sizeof($dados) == 0)
            return null;
        else
            $id = $dados[0]->id;

        return $id;
    }

  
}
