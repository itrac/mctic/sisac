<?php

require_once "BaseModel.php";

class UsuarioEventoModel extends BaseModel{
    protected $tabela = "usuario_evento";

    public function recuperarStatus($id_usuario, $id_evento){
        $usuario = $this->db->get_where($this->tabela,['id_usuario'=>$id_usuario,'id_evento'=>$id_evento])->row();
        if($usuario != null){
            return true;
        }else{
            return false;
        }
    }

    public function atualizarAlteracoes($dados){
        $this->db->where('id_usuario',$dados['id_usuario']);
        $this->db->where('id_evento',$dados['id_evento']);
        $this->db->update($this->tabela, $dados);
    }

    public function usuariosAtualizadosNoEvento($id_evento){
        return $this->db->get_where($this->tabela,['id_evento'=>$id_evento])->num_rows();
    }

    private function carregarStatusUser($id_evento, $usuario){
        if($this->UsuarioEventoModel->recuperarStatus($usuario->id, $id_evento)){
            $usuario->status = "Atualizado";
        }else{
            $usuario->status = "Não atualizado";
        }
    }

    public function carregarStatus($id_evento, $usuarios){
        foreach($usuarios as $usuario){
            $this->carregarStatusUser($id_evento,$usuario);
        }
    }

    public function carregarStatusValidos($id_evento, $usuarios){
        $usuariosValidos = array();
        foreach ($usuarios as $usuario) {
            if($usuario->valido == 1){
                array_push($usuariosValidos,$usuario);
            }
        }
        $this->carregarStatus($id_evento, $usuariosValidos);
        return $usuariosValidos;
    }

}

?>
