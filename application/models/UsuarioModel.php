<?php

require_once "BaseModel.php";

class UsuarioModel extends BaseModel{
    protected $tabela = "usuario";

    public function recuperarPorNomeUsuario($nomeUsuario){
        $usuario = $this->db->get_where($this->tabela,['nome_usuario'=>$nomeUsuario])->row();
        return $usuario;
    }

	public function encontraEmpresaDeUsuario($idDaEmpresaDoUsuario){
		$empresaDoUsuario = $this->db->query("SELECT * FROM empresa WHERE id = '$idDaEmpresaDoUsuario'")->row()->empresa;
		return $empresaDoUsuario;
	}

	public function encontraCargoDoUsuario($idDoCargoDoUsuario){
		$cargoDoUsuario = $this->db->query("SELECT * FROM cargo WHERE id = '$idDoCargoDoUsuario'")->row()->cargo;
		return $cargoDoUsuario;
	}

	public function encontraBlocoDoUsuario($isDaEmpresaDoUsuario){
		$blocoUsuario = $this->db->query("SELECT * FROM bloco WHERE id = '$isDaEmpresaDoUsuario'")->row()->bloco;
		return $blocoUsuario;
	}

	public function encontrarCoordenacaoDoUsuario($idDaCoordenacaoDoUsuario){
		$coordenacaoDoUsuario = $this->db->query("SELECT * FROM lotacao WHERE id = '$idDaCoordenacaoDoUsuario'")->row()->lotacao;
		return $coordenacaoDoUsuario;
	}

	public function encontrarOcupacaoDoUsuario($idDaLotacaoDoUsuario){
		$ocupacaoDoUsuario = $this->db->query("SELECT * FROM ocupacao WHERE id = '$idDaLotacaoDoUsuario'")->row()->ocupacao;
		return $ocupacaoDoUsuario;
	}
  public function encontrarSetorDoUsuario($idDoSetorDoUsuario){
		$setorDoUsuario = $this->db->query("SELECT * FROM setor WHERE id = '$idDoSetorDoUsuario'")->row()->setor;
		return $setorDoUsuario;
	}
    public function atualizarUltimoAcesso($id_usuario){
        date_default_timezone_set('America/Sao_Paulo');
		$date = date('Y-m-d H:i:s');
        $dados = array('ultimo_acesso' => $date);
        $this->db->where('id',$id_usuario);
        $this->db->update($this->tabela, $dados);
    }
}

?>
