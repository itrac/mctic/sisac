<style>
    .loader-usuarios {
        position: relative;
        top: 50%;
        border: 16px solid #E5E5E5; /* Light grey */
        border-top: 16px solid #3498db; /* Blue */
        border-radius: 50%;
        width: 80px;
        height: 80px;
        animation: spin 1.5s linear infinite;
        align: center;
    }
    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
</style>
<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
        	<?php if($this->session->evento->id != -1) :  ?>
            	<h3 class="box-title">Lista de Usuários para o Evento: <?php echo $this->session->evento->evento; ?></h3>
        	<?php else : ?>
				<h3 class="box-title">Não há eventos ativos no momento.</h3>
        	<?php endif; ?>
        </div>
        <div class="box-body">
        <center><div id="loader-usuarios" class="loader-usuarios"></div></center>
        <?php if(@$usuarios): ?>
			<div id="tableContainer" hidden="true">
			    <div style=''>
				    <label><input type="radio" name="filtro" value="Atualizado">&nbsp;Atualizados&nbsp;&nbsp;</label>
					<label><input type="radio" name="filtro" value="Não atualizado">&nbsp;Não atualizados&nbsp;&nbsp;</label>
					<label><input type="radio" name="filtro" value="Sem filtro">&nbsp;Sem Filtro&nbsp;&nbsp;</label>
				</div>
			<table id="listaUsuarios" class="table table-striped table-bordered" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th> Nome </th>
						<th> Nome de Usuário </th>
						<th> E-mail institucional </th>
						<th> Cargo </th>
						<th> Status </th>
						<th> Ações </th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($usuarios as $usuario) {
						if($usuario->email_institucional){
							$emailInstitucional = $usuario->email_institucional;
						}
						else{
							$emailInstitucional = "<p class='text-danger'>Usuário não possui e-mail institucional</p";
						}
						echo "<tr>
								<td>". $usuario->nome ."</td>
								<td>". $usuario->nome_usuario . "</td>
								<td>". $emailInstitucional . "</td>
								<td>". $cargos[$usuario->id] . "</td>
								<td>". $usuario->status . "</td>
								<td>
									<center>
										<a href='". base_url("Usuario/editarUsuario?nome_usuario=" . $usuario->nome_usuario). "'>
											<button class='btn btn-primary'>
												<i class='glyphicon glyphicon-edit'></i>
												Editar
											</button>
										</a>
									</center>
								</td>
							 </tr>";
					}?>
				</tbody>
			</table>
			<div>

        <?php else: ?>
            <div class="text-center alert alert-info">Nenhum usuário cadastrado.
            </div>
        <?php endif; ?>

        </div>
    </div>
</section>

<script>

$(function(){
	<?php if(isset($_GET['editado'])): ?>
		var mensagem = "Edição realizada com sucesso!";
		toastr.success(mensagem, '', { timeOut: 5500 });
	<?php endif; ?>
	
	$('#listaUsuarios').DataTable(dataTableConfig);

    $("#loader-usuarios").hide();
	$('#tableContainer').removeAttr("hidden");
});

$('input[type="radio"]').on('click', function(e) {
	var isFirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
	$.fn.dataTableExt.afnFiltering.length = 0;
	if(isFirefox){
		filter(e.target.attributes[1].nodeValue);
	}
	else{
		filter(e.target.attributes[2].nodeValue);
	}
	$('#listaUsuarios').dataTable().fnDraw();
});

var filter = function(filterValue) {
	$.fn.dataTableExt.afnFiltering.push(
	   	function( oSettings, aData, iDataIndex ) {
			if (aData[4] == filterValue || filterValue == 'Sem filtro'){
	          	return true;
			} else {
	            return false;
     		}
        }
	);
};

</script>
