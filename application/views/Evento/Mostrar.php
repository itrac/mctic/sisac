<html>
<head>
	<script type="text/javascript" src="<?php echo base_url("vendor/dataTables/dataTables-config.js") ?>"></script>
	<script type="text/javascript" src="<?php echo base_url("vendor/dataTables/jquery.dataTables.min.js") ?>"></script>
	<script type="text/javascript" src="<?php echo base_url("vendor/dataTables/dataTables.bootstrap.min.js") ?>"></script>
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			$('#lista_de_eventos').DataTable(dataTableConfig);
		} );
	</script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url("vendor/dataTables/dataTables.bootstrap.min.css") ?>">
</head>
<body>
	<?php     
	    $browser = '';
	    $ua = strtolower($_SERVER['HTTP_USER_AGENT']);
	    if (preg_match('~(?:msie ?|trident.+?; ?rv: ?)(\d+)~', $ua, $matches)) $browser = 'ie';
	    elseif (preg_match('~(safari|chrome|firefox)~', $ua, $matches)) $browser = $matches[1];
	?>
	<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Lista de Eventos</h3>
        </div>
        <div class="box-body">
        <?php if(@$eventos): ?>
					<table id="lista_de_eventos" class="table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>Nome</th>
								<th>Data de Início</th>
								<th>Data de Término</th>
								<th>Atualizados/Total</th>
								<th>Ações</th>
								<th>Gerar Relatório</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($eventos as $evento): ?>
								<tr>
									<td style="vertical-align: middle"><?php echo $evento->evento ?></td>
									<td style="vertical-align: middle"><?php echo $evento->data_inicio?></td>
									<td style="vertical-align: middle"><?php echo $evento->data_fim?></td>
									<td style="vertical-align: middle"><?php echo $evento->numeroDeAtualizados?> / <?php echo ($evento->quantidade_usuarios == NULL) ? "Ainda não foram importados usuários para esse evento." : $evento->quantidade_usuarios?></td>
									<td style="text-align: center; vertical-align: middle">
									<?php if($usuario->id_perfil==1): ?>
										<button onclick="preencheModalComEvento(this);" data-id="<?php echo $evento->id?>" data-evento="<?php echo $evento->evento?>" data-inicio="<?php echo $evento->data_inicio?>"
												data-fim="<?php echo $evento->data_fim?>" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
												<i class="glyphicon glyphicon-edit"></i> Editar
										</button>
										<?php endif; ?>
									</td>
									<td style="text-align: center; vertical-align: middle">
										<?php if ($usuario->id_perfil != 3): ?>
											<a>
												<?php if($browser == 'ie') : ?>
													<button onclick="redirecionarParaRelatorio('<?php echo $evento->id?>')" class="btn btn-warning" data-target="#relatorios"
														<i class="glyphicon glyphicon-list-alt"></i> Gerar Relatório
													</button>
												<?php else : ?>
													<button onclick="redirecionarParaRelatorio('<?php echo $evento->id?>')" class="btn btn-warning" data-toggle="modal" data-target="#relatorios"
														<i class="glyphicon glyphicon-list-alt"></i> Gerar Relatório
													</button>
												<?php endif; ?>
											</a>
										<?php endif; ?>
									</td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
        <?php else: ?>
            <div class="text-center alert alert-info">Nenhum evento criado até o momento.
            </div>
        <?php endif; ?>

        </div>
    </div>
	    <?php if($usuario->id_perfil==1): ?>
			<div>
				<button onclick="alterarModalParaCriar();" class="btn btn-success" data-toggle="modal" data-target="#myModal">
					<i class="fa fa-plus-circle"></i> Criar Evento
				</button>
			</div>
		<?php endif; ?>
		<!-- Modal -->
		<div class="modal fade" id="myModal" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" onclick="zerarModal();" class="close" data-dismiss="modal">&times;</button>
						<h4 id="titulo_cabecalho" class="modal-title">Criar Evento</h4>
					</div>
					<form id="eventoForm"  onsubmit="return validaEvento();" action="./evento/criarEvento" method="post">
						<div class="modal-body">
							<div class="form-group">
								<label class="control-label" ><b>Digite o nome do evento: </b></label>
								<input class="form-control" type="text" placeholder="Digite o nome do evento" name="nomeEvento" id="nomeEvento" required>
							</div>
							<div class="form-group">
								<p>Data de Início: <input type="text" id="dataInicio" name="dataInicio" required/></p>
							</div>
							<div class="form-group">
								<p>Data de Término: <input type="text" id="dataFim" name="dataFim" required/></p>
							</div>
							<div class="form-group" hidden>
								<input class="form-control" type="text" name="idEvento" id="idEvento">
							</div>
						</div>

						<div style="margin:20px; display:none;" id="erros" class="alert alert-danger">
						</div>
						<div class="modal-footer">
							<button id="submitEvento" type="submit" class="btn btn-default">Criar Evento</button>
						</div>
					</form>
			  </div>
			</div>
	  </div>
	</section>
	<script>
		function converterDataPadraoIngles(data){
			var [dia, mes, ano] = data.split('/');
			var dataFormatada = mes + "/" + dia + "/" + ano;
			return dataFormatada;
		}

	    function validaDataEntreEventos(dataInicial, dataFinal){
	      var textoDoErro = "- Escolha um intervalo de datas que não esteja sendo usado por outro evento. <br>";

	      <?php echo "var eventos = " . json_encode($eventos) . "\n"; ?>

	      console.log("Evento no Atual:" + dataInicial + " " + dataFinal);
	      var dataInicioEventoAtual = new Date(converterDataPadraoIngles(dataInicial));
	      var dataFimEventoAtual = new Date(converterDataPadraoIngles(dataFinal));

	      var dataInicioEventoBanco;
	      var dataFimEventoBanco;
	      var dataValida = true;

	      for (var i = 0; i < eventos.length; i++) {
	        console.log("Evento: " + i + " no Banco:" + eventos[i].data_inicio + " " + eventos[i].data_fim);

	        dataInicioEventoBanco = new Date(converterDataPadraoIngles(eventos[i].data_inicio));
	        dataFimEventoBanco = new Date(converterDataPadraoIngles(eventos[i].data_fim));
	        if((dataInicioEventoAtual > dataFimEventoBanco &&
	            dataFimEventoAtual > dataFimEventoBanco) ||
	           (dataInicioEventoAtual < dataInicioEventoBanco &&
	            dataFimEventoAtual < dataInicioEventoBanco)){
	          dataValida = true;
	        }else{
	          if(document.getElementById("submitEvento").innerHTML == "Editar Evento"){
	            console.log(eventos[i].id + "  " + document.getElementById("idEvento").value);
	            if(eventos[i].id != document.getElementById("idEvento").value){
	              console.log("Data inicial inválida");
	              escreverErro(textoDoErro);
	              dataValida = false;
	              return false;
	            }else{
	              dataValida = true;
	            }
	          }else{
	            console.log("Data inicial inválida");
	            escreverErro(textoDoErro);
	            dataValida = false;
	            return false;
	          }
	        }
	      }

	      if (dataValida){
	        console.log("Data inicial válida");
	        apagarErro(textoDoErro);
	        return true;
	      }
	    }

	    function validaNomeUnico(nomeEvento){
	      var textoDoErro = "- O Nome do evento deve ser único. <br>";
	      var nomeEventoFormatado = nomeEvento.trim();

	      <?php echo "var eventos = " . json_encode($eventos) . "\n"; ?>
	      console.log(eventos);

	      var nomeValido = true;
	      for (var i = 0; i < eventos.length; i++) {
	        console.log(eventos[i].evento);
	        if(eventos[i].evento.localeCompare(nomeEventoFormatado) != 0){
	          nomeValido = true;
	        }else{
	          if(document.getElementById("submitEvento").innerHTML == "Editar Evento"){
	            console.log(eventos[i].id + "  " + document.getElementById("idEvento").value);
	            if(eventos[i].id != document.getElementById("idEvento").value){
	              console.log("Nome não único");
	              escreverErro(textoDoErro);
	              nomeValido = false;
	              return false;
	            }else{
	              nomeValido = true;
	            }
	          }else{
	            console.log("Nome não único");
	            escreverErro(textoDoErro);
	            nomeValido = false;
	            return false;
	          }
	        }
	      }

	      if (nomeValido){
	        console.log("Nome único");
	        apagarErro(textoDoErro);
	        return true;
	      }
	    }
	</script>
</body>
</html>
