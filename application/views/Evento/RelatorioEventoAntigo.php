<style>
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: #404040;
    color: white;
}
</style>
<section class="content">
    <div class="box box-primary">
        <?php if(@$usuarios): ?>
        <div class="box-header with-border">
            <h3 class="box-title">Lista de Usuários</h3>
        </div>
        <div class="box-body">
            <div id="loader" class="loader"></div>
			<div id="tableContainer" hidden="true">
			<table id="listaUsuarios" class="table table-striped table-bordered" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th> Nome </th>
						<th> Nome de Usuário </th>
						<th> E-Mail </th>
                        <th> Cargo </th>
						<th> Atualizado </th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($usuarios as $usuario) {
						echo "<tr>
								<td>". $usuario->nome ."</td>
								<td>". $usuario->nome_usuario . "</td>
								<td>". $usuario->email_institucional . "</td>
								<td>". $cargos[$usuario->id] . "</td>
                                <td>". $usuario->status . "</td>
							 </tr>";
					}?>
				</tbody>
			</table>
			<div>

        <?php else: ?>
            <div class="box-header with-border">
                <h3 class="box-title">Evento Sem Usuários Importados</h3>
            </div>
            <div class="box-body">
                <div id="loader" class="loader"></div>
    			<div id="tableContainer" hidden="true">
    			<div>
            <div class="text-center alert alert-info">É necessário importar usuários na data do evento desejado.
            </div>
        <?php endif; ?>

        </div>
    </div>
</section>
