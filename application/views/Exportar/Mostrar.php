<style>
    .loader-exportar {
        position: relative;
		margin-top:5%;
        top: 50%;
		display:none;
        border: 16px solid #E5E5E5; /* Light grey */
        border-top: 16px solid #E03030; /* Red */
        border-radius: 50%;
        width: 80px;
        height: 80px;
        animation: spin 1.5s linear infinite;
        align: center;
    }
    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
</style>
<html>
	<head></head>
	<body>
		<?php if(@$mensagem):?>
            <div style="padding:30px;" class="alert"><center><b><?php echo $mensagem; ?></b></center></div>
			<br/>
		<?php else: ?>
			<?php if(empty($dataUltimaExportacao)):?>
				<center style="backgroung-color:#ffffff;"><pre>Nenhuma exportação até o momento.</pre></center>
			<?php else: ?>
				<center style="backgroung-color:#ffffff;"><pre>Última data de exportação: <?php echo $dataUltimaExportacao;?></pre></center>
			<?php endif; ?>
			<div class="form-group">
				<div id="informacaoDeExportar" style="padding:50px;">
					<h1 style="text-align: center;"><span style="color: #ff0000;">Aten&ccedil;&atilde;o</span></h1>
					<h3 style="text-align: center;"><span style="color: #000000;">Para a fun&ccedil;&atilde;o Exportar:</span></h3>
					<h3 style="text-align: center;">O sistema escreverá todos os dados do banco de dados no Active Directory.</h3>
					<h3 style="text-align: center;">Para realizar a restaura&ccedil;&atilde;o ser&aacute; necess&aacute;rio contato com a &aacute;rea respons&aacute;vel pelo AD.</h3>
					<h3 style="text-align: center;">Ap&oacute;s o clique no bot&atilde;o abaixo o procedimento ser&aacute; iniciado.</h3>
					<h3>&nbsp;</h3>
					<h3 style="text-align: center;"><span style="color: #000000;">Se voc&ecirc; estiver totalmente ciente desta fun&ccedil;&atilde;o e das suas consequ&ecirc;ncias clique no bot&atilde;o abaixo:</span></h3>
				</div>
			</div>
		<div class="form-group">
			<center>
				<button class="btn btn-danger" data-toggle="modal" data-target="#exportModal"><i class="fa fa-upload"></i> Exportar para o AD</button>
			</center>
		<?php endif; ?>
			<!-- Modal -->
			<div class="modal fade" id="exportModal" role="dialog">
				<div class="modal-dialog">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<h4 style="text-align: center;" id="titulo_cabecalho" class="modal-title">Confirmar Exportação</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label class="control-label" ><b>Você deseja realmente realizar a exportação dos dados do banco do SisAC para o AD (Active Directory) do MCTIC?</b></label>
							</div>
						</div>
						<div class="modal-footer">
							<form action="<?php echo base_url('exportar/exportarParaOAd')?>" method="POST">
								<center>
									<button onClick="mostraLoaderExportar()"  class="btn btn-danger" id="botao_export"><i class="fa fa-upload"></i> Exportar para o AD</button>
									<button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
									<div id="loader-exportar" class="loader-exportar"></div>
								</center>
							</form>
						</div>
				  </div>
				</div>
		 	</div>
		</div>
		<br>
	</body>
	<script>
		function mostraLoaderExportar(){
			$("#loader-exportar").show();
		}
	</script>
</html>