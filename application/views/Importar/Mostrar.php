<style>
    .loader-importar {
        position: relative;
		margin-top:5%;
        top: 50%;
		display:none;
        border: 16px solid #E5E5E5; /* Light grey */
        border-top: 16px solid #E03030; /* Red */
        border-radius: 50%;
        width: 80px;
        height: 80px;
        animation: spin 1.5s linear infinite;
        align: center;
    }
    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
</style>
<html>
	<head></head>
	<body>
		<?php if(@$mensagem):?>
            <div style="padding:30px;" class="alert"><center><b><?php echo $mensagem; ?></b></center></div>
			<br/>
		<?php else: ?>
			<?php if(empty($dataUltimaImportacao)):?>
				<center style="backgroung-color:#ffffff;"><pre>Nenhuma importação até o momento.</pre></center>
			<?php else: ?>
				<center style="backgroung-color:#ffffff;"><pre>Última data de importação: <?php echo $dataUltimaImportacao;?></pre></center>
			<?php endif; ?>
			<div class="form-group">
				<div id="informacaoDeExportar" style="padding:50px;">
					<h1 style="text-align: center;"><span style="color: #ff0000;">Aten&ccedil;&atilde;o</span></h1>
					<h3 style="text-align: center;"><span style="color: #000000;">Para a fun&ccedil;&atilde;o Importar:</span></h3>
					<h3 style="text-align: center;">O sistema coletar&aacute; todos os dados dos usu&aacute;rios do AD.</h3>
					<h3 style="text-align: center;">Os usu&aacute;rios importados j&aacute; estar&atilde;o dispon&iacute;veis para atualiza&ccedil;&atilde;o a partir da importa&ccedil;&atilde;o.</h3>
					<h3 style="text-align: center;">A importa&ccedil;&atilde;o poder&aacute; ocorrer em qualquer momento, independente de atualiza&ccedil;&otilde;es estarem em curso.</h3>
					<h3 style="text-align: center;">Os dados j&aacute; existentes no sistema ser&atilde;o sobrescritos se estiverem sido atualizados no AD.</h3>
					<h3 style="text-align: center;">Se o sistema j&aacute; tiver atualizado o registro, este n&atilde;o ser&aacute; atualizado com o dado vindo do AD.</h3>
					<p>&nbsp;</p>
					<h3 style="text-align: center;"><span style="color: #000000;">Se voc&ecirc; estiver totalmente ciente desta fun&ccedil;&atilde;o e das suas consequ&ecirc;ncias clique no bot&atilde;o abaixo:</span></h3>
				</div>
			</div>
			<div class="form-group">
				<center>
					<button class="btn btn-danger" data-toggle="modal" data-target="#importModal"><i class="fa fa-download"></i> Importar do AD</button>
				</center>
		<?php endif; ?>
			<!-- Modal -->
			<div class="modal fade" id="importModal" role="dialog">
				<div class="modal-dialog">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<h4 style="text-align: center;" id="titulo_cabecalho" class="modal-title">Confirmar Importação</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label class="control-label" ><b>Você deseja realmente realizar a importação dos dados do AD (Active Directory) do MCTIC para o banco do SisAC?</b></label>
							</div>
						</div>
						<div class="modal-footer">
							<form action="<?php echo base_url('importar/importarUsuarioParaBD')?>" method="POST">
								<center>
									<button onClick="mostraLoader()" class="btn btn-danger" id="botao_export"><i class="fa fa-download"></i> Importar do AD</button>
									<button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
									<div id="loader-importar" class="loader-importar"></div>
								</center>
							</form>
						</div>
				  </div>
				</div>
		 	</div>
		</div>
		<br>
	</body>
	<script>
		function mostraLoader(){
			$("#loader-importar").show();
		}
	</script>
</html>