<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="vendor/twbs/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url("vendor/font-awesome/css/font-awesome.min.css");?>">
    <link rel="shortcut icon" href="<?php echo base_url("img/favicon.ico");?>"/>
    <title>Sistema de Atualização Cadastral</title>
    <style>
    .topo{
        margin-top:10%;
    }
    .panel{
        margin-top:20px;
    }
    .logo-cf{
        margin-top:52px;
    }
    .logo-dpu{
        margin-top:40px;
    }
    html{
        height: 100%;
    }
    body{
        background: linear-gradient(#add8e6, #4682b4) no-repeat;
        background-attachment: fixed;
    }
    .panel > .panel-heading-custom {
        background-color: #343a40;
        color: white;
    }
    .panel.noborder {
        border: none;
        box-shadow: none;
    }
    noscript h1{
        margin: 0 auto;
        width: 60vw;
        margin-top: 20%;
    }
    </style>
</head>
<body>
<noscript><h1 class="text-center">Para o correto funcionamento do sistema Atualizar Dados habilite o JavaScript do navegador.</h1></noscript>
<div class="container" style="display:none">
    <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-8 col-xs-offset-2 topo">
        <div class="text-center">
            <img class="logo-cf" src="./img/logo.png" width="330">
        </div>
        <div class="col-xs-12">
            <div class="panel noborder panel-default">
                <div class="panel-heading panel-heading-custom text-center"> Sistema de Atualização Cadastral </div>
                    <div class="panel-body">
                        <form action="./seguranca/login" method="post">
                            <div class="text-danger text-justify"><b><i class="fa fa-exclamation-triangle"></i> Para acessar o sistema utilize as suas credenciais de acesso à rede do MCTIC.</b></div><br/>
                            <div class="form-group">      
                                <label class="control-label" for="usuario"><b>Nome de usuário</b></label>
                                <div class="input-group">
                                    <input class="form-control" type="text" placeholder="Digite o seu nome de usuário" name="usuario" id="usuario"  autofocus required>
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                </div>
                            </div>
                            <div class="form-group"> 
                                <label class="control-label" for="senha"><b>Senha</b></label>
                                <div class="input-group">
                                    <input class="form-control" type="password" placeholder="Digite a sua senha" name="senha" id="senha" required>
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                </div>
                            </div>
                            <?php if($mensagem):?>
                                <div class="alert alert-danger"><?php echo $mensagem; ?></div>
                            <?php endif; ?>
                            <button type="submit" class="btn btn-success btn-block">
                                <i class="glyphicon glyphicon-log-in"></i>&nbsp;&nbsp;Acessar
                            </button>
                        </form>
                    </div> 
                </div>
            </div>
            <div class="col-xs-9 text-center">
                <img class="logo-dpu" src="./img/logo-mctic.png" width="330">
            </div>
        </div>
    </div>
</div>
<script>
    function click(){
        console.log("clickou no botão");
    }
</script>
<script type="text/javascript" src="<?php echo base_url("vendor/components/jquery/jquery.min.js"); ?>"></script>
<script>
$(function(){
    $(".container").show();
});
</script>
</body>
</html>