<style>
.input-group-addon {
  background-color: #f35c5c;
  color: white;
}
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}
.modal-backdrop
{
    opacity:0.8 !important;
}
#dv_branco {
    height: 55px;
}
</style>
<head>
	<script type="text/javascript" src="<?php echo base_url("vendor/tool_tip/wz_tooltip.js");?>"></script>
</head>
<h2 class="title text-center">Formulário para Atualização Cadastral</h2>
<div class="panel panel-default">
    <div class="panel-body">
        <form class="form" id="formulario" method="post" action="<?php echo base_url("formulario/salvar"); ?>">
            <input type="hidden" name="id" value="<?php echo @$usuario->id?>"/>
            <?php if(@$mensagem):?>
                <div class="alert alert-info"><?php echo $mensagem; ?></div>
            <?php endif; ?>
            <?php if(@$validacao):?>
                <div class="alert alert-danger"><?php echo $validacao; ?></div>
			<?php endif; ?>

            <div class="row">
                <div class="col-md-12">
                    <p class="text-primary text-center"><b>Para obter mais detalhes sobre algum campo do formulário basta posicionar o mouse sobre a interrogação.</b></p>
                </div>
            </div>
            <div class="row">
            <div class="col-md-6">
            <!-- Início da DIV de Nome -->
            <div class="row">
                <div class="col-md-12">
                    <label class="control-label" for="nome"><b>Nome Completo</b></label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-11 col-xs-10">
                    <input class="form-control" type="text" placeholder="Digite o seu nome completo" name="nome" id="nome" value="<?php echo $usuario->nome ?>" required minlength="3">
                </div>

				<img src="<?php echo base_url('img/interrogacao.png'); ?>" onmouseover="Tip('Digite o seu completo, nome e sobrenomes, sem abreviar.',WIDTH,370,FONTSIZE,'11pt',TEXTALIGN,'justify');" onmouseout="UnTip()">
            </div>
            <!-- Fim da DIV de Nome -->
            <br/>
            <!-- Início da DIV de E-mail institucional -->
            <div class="row">
                <div class="col-md-12">
                    <label class="control-label" for="email_institucional"><b>E-mail institucional</b></label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-11 col-xs-10">
                <input class="form-control" type="email" placeholder="Digite o seu e-mail institucional" name="email_institucional" id="email_institucional" value="<?php echo $usuario->email_institucional ?>">
                </div>
				<img src="<?php echo base_url('img/interrogacao.png'); ?>" onmouseover="Tip('Digite o e-mail fornecido a você pelo MCTIC. Geralmente com o termo @mctic. Caso você não possua um e-mail institucional, poderá deixar o campo em branco.',WIDTH,370,FONTSIZE,'11pt',TEXTALIGN,'justify');" onmouseout="UnTip()">
            </div>
            <!-- Fim da DIV de E-mail institucional -->
            <br/>
            <!-- Início da DIV de e-mail pessoal -->
            <div class="row">
                <div class="col-md-12">
                    <label class="control-label" for="email_pessoal"><b>E-mail pessoal</b></label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-11 col-xs-10">
                    <input class="form-control" type="email" placeholder="Digite o seu e-mail pessoal" name="email_pessoal" id="email_pessoal" value="<?php echo $usuario->email_pessoal ?>" required>
                </div>
				<img src="<?php echo base_url('img/interrogacao.png'); ?>" onmouseover="Tip('Digite a sua conta de e-mail pessoal. Servirá para resetar a sua senha do e-mail do MCTIC. Caso não informado, o reset de senha, caso necessário, deverá ser realizado presencialmente nas dependências da CGTI.',WIDTH,370,FONTSIZE,'11pt',TEXTALIGN,'justify');" onmouseout="UnTip()">
            </div>
            <!-- Fim da DIV de e-mail pessoal  -->
            <br/>
            <!-- Início da DIV de Telefone Institucional Completo -->
            <div class="row">
                <div class="col-md-12">
                    <label class="control-label" for="telefone"><b>Telefone Institucional completo com DDD</b></label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-11 col-xs-10">
                <input class="form-control" type="text" placeholder="Digite o seu telefone Institucional completo" name="telefone" id="telefone" value="<?php echo $usuario->telefone ?>" required>
                </div>
				<img src="<?php echo base_url('img/interrogacao.png'); ?>" onmouseover="Tip('Digite o seu telefone Institucional completo, apenas os números. Deve-se conter 10 algarismos, sendo 2 para o DDD, 4 para o prefixo e 4 para o ramal.',WIDTH,370,FONTSIZE,'11pt',TEXTALIGN,'justify');" onmouseout="UnTip()">
            </div>
            <!-- Fim da DIV de Telefone Completo -->
            
            <br/>
            <!-- Início da DIV de Telefone Pessoal Completo -->
            <div class="row">
                <div class="col-md-12">
                    <label class="control-label" for="telefone_pessoal"><b>Telefone Pessoal completo com DDD (opcional)</b></label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-11 col-xs-10">
                <input class="form-control" type="text" placeholder="Digite o seu telefone Pessoal completo" name="telefone_pessoal" id="telefone_pessoal" value="<?php echo $usuario->telefone_pessoal ?>" >
                </div>
                <img src="<?php echo base_url('img/interrogacao.png'); ?>" onmouseover="Tip('Digite o seu telefone Pessoal completo, apenas os números. Deve-se conter 10 ou 11 algarismos, sendo 2 para o DDD.',WIDTH,370,FONTSIZE,'11pt',TEXTALIGN,'justify');" onmouseout="UnTip()">
            </div>
            <!-- Fim da DIV de Telefone Completo -->
            <br/>
            <!-- Início da DIV de CPF -->
            <div class="row">
                <div class="col-md-12">
                    <label class="control-label" for="cpf"><b>CPF</b></label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-11 col-xs-10">
                <input class="form-control" type="text" placeholder="Digite o seu CPF" name="cpf" id="cpf" value="<?php echo $usuario->cpf ?>" required>
                </div>
				<img src="<?php echo base_url('img/interrogacao.png'); ?>" onmouseover="Tip('Digite o seu CPF, apenas os números. Sem traços e pontos. Deve-se conter 11 algarismos.',WIDTH,370,FONTSIZE,'11pt',TEXTALIGN,'justify');" onmouseout="UnTip()">
            </div>
            <!-- Fim da DIV de CPF -->
            <br/>
            <!-- Início da DIV de Ocupação -->
            <div class="row">
                <div class="col-md-12">
                    <label class="control-label" for="id_ocupacao"><b>Ocupação</b></label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-11 col-xs-10">
                    <?php foreach($ocupacoes as $ocupacao):?>
                    <label class="radio-inline">
                        <input type="radio" name="id_ocupacao" id="ocupacao" value="<?php echo $ocupacao->id?>" onclick="exibirOcultar(this)" <?php echo ($usuario->id_ocupacao==$ocupacao->id)?'checked':'' ?> required><?php echo $ocupacao->ocupacao ?>
                    </label>
                    <?php endforeach ?>
                </div>
				<img src="<?php echo base_url('img/interrogacao.png'); ?>" onmouseover="Tip('Selecione a sua ocupação dentro do MCTIC.',FONTSIZE,'11pt',TEXTALIGN,'justify');" onmouseout="UnTip()">
            </div>
            <!-- Fim da DIV de Ocupação -->
            <div class="" id="dv_branco">
                <!-- Início da DIV de Cargos Servidor Comissionado -->
                <div id="dv_cargos">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="control-label" for="id_servidor_comissionado"><b>Servidor Comissionado</b></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-11 col-xs-10">
                            <select id="id_servidor_comissionado" class="form-control selectpicker" data-live-search="true" name="id_servidor_comissionado" required>
                                <?php foreach($cargos as $cargo): ?>
                                    <option value="<?php echo $cargo->id?>" <?php echo ($usuario->id_cargo==$cargo->id)?'selected':'' ?>><?php echo $cargo->cargo ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <img src="<?php echo base_url('img/interrogacao.png'); ?>" onmouseover="Tip('Selecione o seu cargo de servidor comissionado.',WIDTH,370,FONTSIZE,'11pt',TEXTALIGN,'justify');" onmouseout="UnTip()">
                    </div>
                </div>
                <!-- Fim da DIV de Cargos Servidor Comissionado -->
                <!-- Início da DIV de Cargos Comissionado -->
                <div id="dv_cargos2">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="control-label" for="id_comissionado"><b>Comissionado</b></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-11 col-xs-10">
                            <select id="id_comissionado" class="form-control selectpicker" data-live-search="true" name="id_comissionado" required>
                                <?php foreach($cargos as $cargo): ?>
                                    <?php if($cargo->id < 7): ?>
                                        <option value="<?php echo $cargo->id?>" <?php echo ($usuario->id_cargo==$cargo->id)?'selected':'' ?>><?php echo $cargo->cargo ?></option>
                                    <?php endif; ?>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <img src="<?php echo base_url('img/interrogacao.png'); ?>" onmouseover="Tip('Selecione o seu cargo de servidor comissionado.',WIDTH,370,FONTSIZE,'11pt',TEXTALIGN,'justify');" onmouseout="UnTip()">
                    </div>
                </div>
                <!-- Fim da DIV de Cargos Comissionado -->
                <!-- Início da DIV de Empresa -->
                <div id="dv_empresa">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="control-label" for="id_empresa"><b>Digite a empresa que você trabalha</b></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-11 col-xs-10">
                            <input class="form-control" type="text" placeholder="Digite a empresa que você trabalha" name="id_empresa" id="id_empresa" value="<?php echo (isset($nomeEmpresa->empresa))?$nomeEmpresa->empresa:"" ?>" class='auto'>
                        </div>
                        <img src="<?php echo base_url('img/interrogacao.png'); ?>" onmouseover="Tip('Como você selecionou a opção de ocupação <u>Terceirizado</u>, digite o nome da empresa na qual você trabalha.',WIDTH,370,FONTSIZE,'11pt',TEXTALIGN,'justify');" onmouseout="UnTip()">
                    </div>
                </div>
                <!-- Fim da DIV de Empresa -->
                <!-- Início da DIV de Especificação de outra ocupação -->
                <div id="dv_ocupacao">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="control-label" for="outra_ocupacao"><b>Especifique a sua ocupação</b></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-11 col-xs-10">
                            <input class="form-control" type="text" placeholder="Especifique a sua ocupação no MCTIC" name="outra_ocupacao" id="outra_ocupacao" value="<?php echo $usuario->outra_ocupacao ?>">
                        </div>
                        <img src="<?php echo base_url('img/interrogacao.png'); ?>" onmouseover="Tip('Como você selecionou a opção de ocupação Outros, digite a sua ocupação no MCTIC.',WIDTH,370,FONTSIZE,'11pt',TEXTALIGN,'justify');" onmouseout="UnTip()">
                    </div>
                </div>
                <!-- Fim da DIV de Especificação de outra ocupação -->
            </div>
            <br/>

            </div> <!-- Fim da DIV de quebra de coluna 1 -->

            <div class="col-md-6">
            <br/>
            <!-- Início da DIV de lotação -->
            <div id="dv_lotacao">
                <div class="row">
                    <div class="col-md-12">
                        <label class="control-label" for="id_lotacao"><b>Lotação</b></label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11 col-xs-10">
                        <select id="id_lotacao" class="form-control selectpicker" data-live-search="true" name="id_lotacao" required>
                            <option></option>
                            <?php foreach($lotacoes as $lotacao): ?>
                                <option value="<?php echo $lotacao->id?>" <?php echo ($usuario->id_lotacao==$lotacao->id)?'selected':'' ?>><?php echo $lotacao->lotacao ." - ". $lotacao->unidade ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
					<img src="<?php echo base_url('img/interrogacao.png'); ?>" onmouseover="Tip('Selecione a sua lotação no MCTIC, seguindo a hierarquia atualizada.',WIDTH,370,FONTSIZE,'11pt',TEXTALIGN,'justify');" onmouseout="UnTip()">
                </div>
            </div>
            <!-- Fim da DIV de lotação -->
            <br/>
            <!-- Início da DIV de sala -->
            <div class="row">
                <div class="col-md-12">
                    <label class="control-label" for="sala"><b>Sala</b></label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-11 col-xs-10">
                <input class="form-control" type="text" placeholder="Digite a sua sala" name="sala" id="sala" value="<?php echo $usuario->sala ?>" required>
                </div>
				<img src="<?php echo base_url('img/interrogacao.png'); ?>" onmouseover="Tip('Digite a sala que você trabalha no MCTIC.',FONTSIZE,'11pt',TEXTALIGN,'justify');" onmouseout="UnTip()">
            </div>
            <!-- Fim da DIV de sala -->
            <br/>
            <!-- Início da DIV de andar -->
            <div class="row">
                <div class="col-md-12">
                    <label class="control-label" for="andar"><b>Andar</b></label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-11 col-xs-10">
                <input class="form-control" type="text" placeholder="Digite o seu andar" name="andar" id="andar" value="<?php echo $usuario->andar ?>" required>
                </div>
				<img src="<?php echo base_url('img/interrogacao.png'); ?>" onmouseover="Tip('Digite o andar que você trabalha no MCTIC.',FONTSIZE,'11pt',TEXTALIGN,'justify');" onmouseout="UnTip()">
            </div>
            <!-- Fim da DIV de andar -->
            <br/>
            <!-- Início da DIV de bloco -->
            <div id="dv_bloco">
                <div class="row">
                    <div class="col-md-12">
                        <label class="control-label" for="id_bloco"><b>Bloco/Regional</b></label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11 col-xs-10">
                        <select class="form-control selectpicker" data-live-search="true"  onchange="exibir()"  name="id_bloco" id="bloco" required>
                            <option></option>
                            <?php foreach($blocos as $bloco): ?>
                                <option name="bloco" value="<?php echo $bloco->id?>" <?php echo ($usuario->id_bloco==$bloco->id)?'selected':'' ?>><?php echo $bloco->bloco ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
					<img src="<?php echo base_url('img/interrogacao.png'); ?>" onmouseover="Tip('Selecione em qual bloco ou regional do MCTIC você trabalha.',WIDTH,370,FONTSIZE,'11pt',TEXTALIGN,'justify');" onmouseout="UnTip()">
                </div>
            </div>
            <!-- Fim da DIV de bloco -->

            <!-- Inicio da DIV de setor -->
            <div id="dv_setor">
            <br/>
                <div class="row">
                    <div class="col-md-12">
                        <label class="control-label" for="id_setor"><b>Setor</b></label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11 col-xs-10">
                        <select class="form-control selectpicker" data-live-search="true" name="id_setor" id="setor" required>
                            <option></option>
                            <?php foreach($setores as $setor): ?>
                              <?php if($setor->id != 3): ?>
                                <option id="option-id-setor" name="setor" value="<?php echo $setor->id?>" <?php echo ($usuario->id_setor==$setor->id)?'selected':'' ?> ><?php echo $setor->setor ?></option>
                              <?php endif ?>
                            <?php endforeach ?>
                        </select>
                    </div>
					<img src="<?php echo base_url('img/interrogacao.png'); ?>" onmouseover="Tip('Selecione em qual setor do MCTIC você trabalha.',WIDTH,370,FONTSIZE,'11pt',TEXTALIGN,'justify');" onmouseout="UnTip()">
                </div>
            </div>
            <!-- Fim da DIV de setor -->

            <!-- Início da DIV de estacao -->
            <br/>
            <div class="row">
                <div class="col-md-12">
                    <label class="control-label" for="estacao"><b>Estação de Trabalho</b></label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-11 col-xs-10">
                    <div class="input-group">
                        <div class="input-group-addon" style="background-color:#f2f2f2; color:#a6a6a6">EST</div>
                        <input class="form-control" type="text" placeholder="Digite o número da sua Estação de Trabalho" name="estacao" id="estacao" value="<?php echo $usuario->estacao ?>" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57"onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" required>
                    </div>
                </div>
				<img src="<?php echo base_url('img/interrogacao.png'); ?>" onmouseover="Tip('Digite o número do seu computador. Você pode encontrá-lo no papel de parede do seu computador (Área de Trabalho), no canto superior direito, campo: Nome do Computador, que inicia com as letras: EST.',WIDTH,370,FONTSIZE,'11pt',TEXTALIGN,'justify');" onmouseout="UnTip()">
            </div>
            <!-- Fim da DIV de estacao -->

            <br/>
            <?php if ($this->session->usuario->id_perfil == 1): ?>
            <!-- Início da DIV de perfil -->

                <div id="dv_perfil"  >
                    <div class="row">
                        <div class="col-md-12">
                            <label class="control-label" for="id_perfil"><b>Perfil</b></label>
                        </div>
                    </div>
					<div class="row">
						<div class="col-md-11 col-xs-10">
                            <select class="form-control selectpicker" data-live-search="true" name="id_perfil" id="id_perfil" required>
                                <option></option>
                                <?php foreach($perfis as $perfil): ?>
                                    <option name="id_perfil" value="<?php echo $perfil->id?>" <?php echo ($usuario->id_perfil==$perfil->id)?'selected':'' ?>><?php echo $perfil->perfil ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <img src="<?php echo base_url('img/interrogacao.png'); ?>" onmouseover="Tip('Selecione o perfil de usuário desejado. Lembrando que cada perfil tem um nível de acesso dentro do sistema.',WIDTH,370,FONTSIZE,'11pt',TEXTALIGN,'justify');" onmouseout="UnTip()">
                    </div>
				</div>
            <!-- Fim da DIV de perfil -->
			<?php else: $id_perfil = $usuario->id_perfil;?>
                <div class="row">
                    <div class="col-md-12">
                        <label class="control-label"></label>
                    </div>
                </div>
				<div class="row">
					<div class="col-md-11 col-xs-10">
						<input type="hidden" name="id_perfil" value="<?php echo $usuario->id_perfil ?>" />
					</div>
				</div>
			<?php endif; ?>
            </div> <!-- Fim da DIV de quebra de coluna 2 -->
            </div><!-- Fim da DIV row geral -->
            <?php if($this->session->evento->id == -1 ) : ?>
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-center text-danger" ><b>Não há eventos ativos no momento, portanto não é possível atualizar o registro. </b></p>
                    </div>
                </div>
            <?php else : ?>
                <button type="submit" class="btn btn-success">
                    <i class="glyphicon glyphicon-save"></i> Enviar
                </button>
            <?php endif; ?>
        </form>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p class="text-center"><b>Em caso de dúvidas no preenchimento, entrar em contato com a Central de Atendimento de Usuário no (61) 2027-6070.</b></p>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog" data-backdrop="static">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" onclick="location.href = '<?php echo base_url("/seguranca/logout");?>';">&times;</button>
          <h4 class="modal-title text-sucess">Atualização cadastral realizada com sucesso! <i class="fa fa-check" style="color:green"></i></h4>
        </div>
        <div class="modal-body">
          <p class="text-info"><b>Seus dados foram atualizados com sucesso!<br/>A Coordenação-Geral de Serviços de Tecnologia da Informação agradece e tenha um ótimo dia.<b></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal" onclick="location.href = '<?php echo base_url("/seguranca/logout");?>';"><i class="glyphicon glyphicon-log-out"></i> Sair</button>
        </div>
      </div>

    </div>
  </div>
<script>
	function mostraAjuda(nomeCampo){
		document.getElementById(nomeCampo).style.display = "inline-block";
	}

	function escondeAjuda(nomeCampo){
		document.getElementById(nomeCampo).style.display = "none";
	}
</script>
<script>
    $('#id_lotacao option').each(function() {
        var minhaString = $(this).text();
        if(minhaString.length > 80){
            $(this).text(minhaString.substring(0,80) + ' ...');
        }
    });
    $('#id_cargo option').each(function() {
        var minhaString = $(this).text();
        if(minhaString.length > 80){
            $(this).text(minhaString.substring(0,80) + ' ...');
        }
    });

    $(function() {

        $("#id_empresa").autocomplete({
            source: "<?php echo base_url("Formulario/recuperarEmpresas") ?>",
            minLength: 1
        });

        var setor = "<?php echo $usuario->id_setor; ?>";
        var mensagem = "<?php echo $mensagem; ?>";
        var id_perfil = "<?php echo $this->session->usuario->id_perfil; ?>";

        if((mensagem == "Atualização Cadastral realizada com sucesso!") && (id_perfil==3)){
            $('#myModal').modal('show');
        }

        $('#dv_cargos').hide();
        $('#dv_cargos2').hide();
        $('#dv_empresa').hide();
        $('#dv_ocupacao').hide();

        var id_ocupacao = $("input[name='id_ocupacao']:checked").val();
        //Servidor Comissionado
        if(id_ocupacao == 2){
            $('#dv_cargos').show();
            $('#dv_cargos select').attr("required","required");
            $('#id_servidor_comissionado').attr("required","required");          
        }else{
            $('#dv_cargos').hide();
            $('#dv_cargos select').removeAttr("required","required");
            $('#id_servidor_comissionado').removeAttr("required","required");   
        }
        //Comissionado
        if(id_ocupacao == 3){
            $('#dv_cargos2').show();
            $('#dv_cargos2 select').attr("required","required");
            $('#id_comissionado').attr("required","required");  
        }else{
            $('#dv_cargos2').hide();
            $('#dv_cargos2 select').removeAttr("required","required");
            $('#id_comissionado').removeAttr("required","required"); 
        }
        //Terceirizado
        if(id_ocupacao == 4){
            $('#dv_empresa').show();
            $('#dv_empresa').attr("required","required");
        }else{
            $('#dv_empresa').hide();
            $('#dv_empresa').removeAttr("required","required");
            $("#id_empresa").val("");
        }
        //Outros
        if(id_ocupacao == 6){
            $('#dv_ocupacao').show();
            $('#outra_ocupacao').attr("required","required");
        }else{
            $('#dv_ocupacao').hide();
            $('#outra_ocupacao').removeAttr("required","required");
            $("#outra_ocupacao").val("");
        }

        if(setor == "1" || setor == "2"){
          $('#dv_setor').show();
        }
        else{
          $('#dv_setor select').removeAttr("required");
          $('#dv_setor').hide();
        }
        $('form').submit(function(e){
            var email_valido = validaEmails();
            var telefone_institucional = validaTelefoneInstitucional();
            var telefone_pessoal = validaTelefonePessoal();
            var cpf_valido = testaCPF();

            if(email_valido && telefone_institucional && cpf_valido && telefone_pessoal){
                return true;
            }else{
                return false;
            }
        });

        function testaCPF() {
            var strCPF = $('#cpf').val();
            strCPF = strCPF.match(/\d/g).join(""); // remain only numbers

            var Soma;
            var Resto;
            Soma = 0;
            var cpf_valido = null;

            if ( strCPF == "00000000000") cpf_valido = false;
            if ( strCPF == "11111111111") cpf_valido = false;
            if ( strCPF == "22222222222") cpf_valido = false;
            if ( strCPF == "33333333333") cpf_valido = false;
            if ( strCPF == "44444444444") cpf_valido = false;
            if ( strCPF == "55555555555") cpf_valido = false;
            if ( strCPF == "66666666666") cpf_valido = false;
            if ( strCPF == "77777777777") cpf_valido = false;
            if ( strCPF == "88888888888") cpf_valido = false;
            if ( strCPF == "99999999999") cpf_valido = false;
            
            for (i=1; i<=9; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
            Resto = (Soma * 10) % 11;
            
            if ((Resto == 10) || (Resto == 11))  Resto = 0;
            if (Resto != parseInt(strCPF.substring(9, 10)) ) cpf_valido = false;
            
            Soma = 0;
            for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
            Resto = (Soma * 10) % 11;
            
            if ((Resto == 10) || (Resto == 11))  Resto = 0;
            if (Resto != parseInt(strCPF.substring(10, 11) ) ) cpf_valido = false;
            if(cpf_valido == null){
                cpf_valido = true;
            }else if(cpf_valido == false ){
                alert("O CPF não está correto, insira novamente por favor");
                $('#cpf').val("");
            }
            return cpf_valido;
        }

        function validaTelefoneInstitucional(){
            var telefone_institucional_valido = null;
            var telefone_institucional = $('#telefone').val();
            telefone_institucional = telefone_institucional.match(/\d/g).join(""); // only numbers
           
            if(telefone_institucional.length != 10){
                telefone_institucional_valido = false;
                alert("O telefone institucional precisa ter 10 dígitos");
            }else{
                telefone_institucional_valido = true;
            }

            return telefone_institucional_valido;
        }

        function validaTelefonePessoal(){
            var telefone_pessoal_valido = null;
            var telefone_pessoal = $('#telefone_pessoal').val();
            if(!telefone_pessoal){
                telefone_pessoal_valido = true;
            }else{
                telefone_pessoal = telefone_pessoal.match(/\d/g).join(""); // only numbers
        
                if(telefone_pessoal.length != 10 && telefone_pessoal.length != 11){
                    telefone_pessoal_valido = false;
                    alert("O telefone institucional precisa ter 10 ou 11 dígitos");
                    $('#telefone_pessoal').val("");
                }else{
                    telefone_pessoal_valido = true;
                }
            }

            return telefone_pessoal_valido;
        }

        function validaEmails(){
            var email_institucional = $('#email_institucional').val();
            var email_pessoal = $('#email_pessoal').val();
            var institucional_valido = false;
            var pessoal_valido = false;

            if(email_institucional != 0){
                if(validarEmail(email_institucional)){
                    institucional_valido = true;
                } else {
                    alert("E-mail institucional inválido");
                    institucional_valido = false;
                }
            }else{
                alert("Digite o E-mail Institucional. É um campo obrigatório");
            }

            if(email_pessoal != 0){
                if(validarEmail(email_pessoal)){
                    pessoal_valido = true;
                } else {
                    alert("E-mail pessoal inválido!");
                    pessoal_valido = false;
                }
            }

            if(institucional_valido && pessoal_valido){
                return true;
            }else{
                return false;
            }
        }

        $("input[id*='cpf']").inputmask({
            mask: ['999.999.999-99'],
            keepStatic: true
        });

        $("input[id='telefone']").inputmask({
            mask: ['(99) 9999-9999'],
            keepStatic: true
        });

        $("input[id='telefone_pessoal']").inputmask({
            mask: ['(99) 99999-9999'],
            keepStatic: false
        });

        $('input[type=radio][name=id_ocupacao]').change(function(){
            var id_ocupacao = $("input[name='id_ocupacao']:checked").val();

            if(id_ocupacao == 2){
                $('#dv_cargos').show();
                $('#id_servidor_comissionado').attr("required","required");
            } else {
                $('#dv_cargos').hide();
                $('#id_servidor_comissionado').removeAttr("required","required");
            }
            if(id_ocupacao == 3){
                $('#dv_cargos2').show();
                $('#id_comissionado').attr("required","required");
            }else{
                $('#dv_cargos2').hide();
                $('#id_comissionado').removeAttr("required","required");
            }
            if(id_ocupacao == 4){
                $('#dv_empresa').show();
                $('#id_empresa').attr("required","required");
            }else{
                $('#dv_empresa').hide();
                $('#id_empresa').removeAttr("required","required");
                $("#id_empresa").val("");
            }
            if(id_ocupacao == 6){
                $('#dv_ocupacao').show();
                $("#outra_ocupacao").attr("required","required");
            }else{
                $('#dv_ocupacao').hide();
                $("#outra_ocupacao").removeAttr("required","required");
                $("#outra_ocupacao").val("");
            }

        });

        $('.selectpicker').selectpicker({
            size: 10
        });

    });

    function validarEmail(email) {
		if(email != ""){
			var regex = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
	        return regex.test(email);
		}else{
			return true;
		}
    }

    function exibirOcultar(rd){
        var ocupacao = $(rd).val();
        if(ocupacao == 2){
            $('#dv_cargos').show();
            $('#dv_cargos select').attr("required","required");
        } else {
            $('#dv_cargos select').removeAttr("required","required");
            $('#dv_cargos').hide();
        }
        if(ocupacao == 3){
            $('#dv_cargos2').show();
            $('#dv_cargos2 select').attr("required","required");
        } else{
            $('#dv_cargos2 select').removeAttr("required","required");
            $('#dv_cargos2').hide();
        }
        if(ocupacao == 4){
            $('#dv_empresa').show();
            $('#dv_empresa').attr("required","required");
        }else{
            $('#dv_empresa').removeAttr("required","required");
            $('#dv_empresa').hide();
            $("#id_empresa").val("");
        }
        if(ocupacao == 6){
            $('#dv_ocupacao').show();
            $('#dv_ocupacao select').attr("required","required");
        }else{
            $('#dv_ocupacao select').removeAttr("required","required");
            $('#dv_ocupacao').hide();
            $("#outra_ocupacao").val("");
        }
    }
    function exibir(){
        var selectBox = document.getElementById("bloco");
        var selectedValue = selectBox.options[selectBox.selectedIndex].value;
        if(selectedValue == 3){
            $('#dv_setor').show();
            $('#dv_setor select').attr("required","required");
        }else{
            $('#dv_setor select').removeAttr("required","required");
            $('#dv_setor').hide();
        }
    }

</script>
