<style>
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: #404040;
    color: white;
}
</style>
<section class="content">
    <div class="box box-primary">
        <div class="box-body">
	        <?php if(@$usuarios): ?>
					<?php foreach ($lotacoes as $lotacao){	
						echo"
						<br/>
						<h3>".$lotacao->lotacao. " - " . $lotacao->unidade."</h3>
						<table class='table table-striped table-bordered' cellspacing='0' width='100%'>
							<thead>
								<tr>
									<th width='30%'> Nome </th>
									<th width='30%'> Nome de Usuário </th>
									<th width='25%'> Telefone </th>
									<th width='15%'> Cargo </th>
								</tr>
							</thead>
						<tbody>";
						$i = 0;
						foreach ($usuarios as $usuario) {
							if(($usuario->id_lotacao == $lotacao->id) && ($usuario->valido == 1)){
								echo
								"<tr>
									<td>". $usuario->nome ."</td>
									<td>". $usuario->nome_usuario . "</td>
									<td>". $usuario->telefone . "</td>
									<td>". $cargos[$usuario->id] . "</td>
								</tr>";
								$i = $i + 1;
							};
						};
						echo"</tbody>
						</table>";
						if($i < 1){
							echo "<center> Não há usuários nesta lotação </center>";
						}
					};?>
	        <?php endif; ?>
        </div>
    </div>
</section>