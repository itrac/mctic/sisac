<style>
    .loader {
        position: relative;
        top: 50%;
        display: none;
        border: 16px solid #E5E5E5; /* Light grey */
        border-top: 16px solid #3498db; /* Blue */
        border-radius: 50%;
        width: 80px;
        height: 80px;
        animation: spin 1.5s linear infinite;
        align: center;
    }
    .loader-results{
       margin: 0 auto; 
       width: 30px; 
       height: 30px; 
       border-top: 16px solid #5cb85c;
    }
    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
</style>  
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Sistema de Atualização Cadastral</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?php echo base_url("vendor/twbs/bootstrap/dist/css/bootstrap.min.css");?>">
  <link rel="stylesheet" href="<?php echo base_url("vendor/font-awesome/css/font-awesome.min.css");?>">
  <link rel="stylesheet" href="<?php echo base_url("vendor/Ionicons/css/ionicons.min.css");?>">
  <link rel="stylesheet" href="<?php echo base_url("vendor/adminLTE/css/AdminLTE.min.css");?>">
  <link rel="stylesheet" href="<?php echo base_url("vendor/adminLTE/css/skins/_all-skins.min.css");?>">
  <link rel="stylesheet" href="<?php echo base_url("vendor/components/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css");?>">
  <link rel="stylesheet" href="<?php echo base_url("vendor/components/jqueryui/themes/base/jquery-ui.min.css");?>">
  <link rel="stylesheet" href="<?php echo base_url("vendor/components/toastr/toastr.min.css");?>">
  <link rel="stylesheet" href="<?php echo base_url("vendor/bootstrap-select/bootstrap-select/dist/css/bootstrap-select.min.css");?>">
  <link rel="shortcut icon" href="<?php echo base_url("img/logo-favicon.ico");?>"/>
  <link rel="stylesheet" href="<?php echo base_url("vendor/dataTables/buttons.dataTables.min.css");?>">
  <link rel="stylesheet" href="<?php echo base_url("vendor/dataTables/dataTables.bootstrap.min.css");?>">
  <link rel="shortcut icon" href="<?php echo base_url("img/favicon.ico");?>"/>
  <style type="text/css" class="init"></style>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="<?php echo base_url("vendor/adminLTE/js/html5shiv.min.js");?>"></script>
  <script src="<?php echo base_url("vendor/adminLTE/js/respond.min.js");?>"></script>
  <![endif]-->

</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php     
    $browser = '';
    $ua = strtolower($_SERVER['HTTP_USER_AGENT']);
    if (preg_match('~(?:msie ?|trident.+?; ?rv: ?)(\d+)~', $ua, $matches)) $browser = 'ie';
    elseif (preg_match('~(safari|chrome|firefox)~', $ua, $matches)) $browser = $matches[1];
?>
<div class="wrapper">
  <header class="main-header">
    <a href="#" class="logo">
      <span class="logo-mini"><b>Sis</b>AC</span>
      <span class="logo-lg"><b>Atualização</b> Cadastral</span>
    </a>
    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <ul class="nav navbar-nav navbar-left">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?=$usuario->nome_usuario?> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url("/seguranca/logout");?>"><i class="glyphicon glyphicon-log-out"></i> Sair</a></li>
                    </ul>
                </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU</li>
        <li class="">
          <a href="<?php echo base_url("/dashboard");?>">
            <i class="fa fa-user-circle"></i> <span>Usuários</span>
          </a>
        </li>

        <li class="">
          <a href="<?php echo base_url("evento");?>">
            <i class="fa fa-calendar"></i> <span>Evento</span>
          </a>
        </li>
          <?php if($browser == 'ie') : ?>
            <li class="">
              <a onClick="mostrarRelatorioLotacao()" data-target="#relatorios">
                <i class="fa fa-clipboard"></i> <span>Relatório de Lotações</span>
              </a>
            </li>
          <?php else : ?>
            <li class="">
              <a onClick="mostrarRelatorioLotacao()" data-toggle="modal" data-target="#relatorios">
                <i class="fa fa-clipboard"></i> <span>  Relatório de Lotações</span>
              </a>
            </li>
          <?php endif; ?>

        <?php if($usuario->id_perfil==1): ?>
          <li class="">
            <a href="<?php echo base_url("exportar");?>">
              <i class="fa fa-upload"></i>
              <span class="text-danger">Exportar SisAC -> AD</span>
            </a>
          </li>
        <?php endif; ?>

        <?php if($usuario->id_perfil==1): ?>
          <li class="">
            <a href="<?php echo base_url("importar");?>">
              <i class="fa fa-download"></i>
              <span class="text-danger">Importar AD -> SisAC</span>
            </a>
          </li>
        <?php endif; ?>
      </ul>
    </section>
  </aside>
  <div class="modal fade" id="relatorios" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-body">
            <div class="form-group">
              <center><div class="loader"></div></center>
						</div>
					</div>	
			  </div>
			</div>
		</div>
  <script type="text/javascript" src="<?php echo base_url("vendor/components/jquery/jquery.min.js"); ?>"></script>

  <script type="text/javascript" src="<?php echo base_url("vendor/components/jqueryui/jquery-ui.min.js"); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url("vendor/components/jqueryui/ui/i18n/datepicker-pt-BR.js"); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url("vendor/components/jquerymask/jquery.inputmask.js"); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url("vendor/components/jquerymask/jquery.validate.min.js"); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url("vendor/twbs/bootstrap/dist/js/bootstrap.min.js"); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url("vendor/components/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url("vendor/components/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.pt-BR.js"); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url("vendor/dataTables/jquery.dataTables.min.js"); ?>"></script> 
  <script type="text/javascript" src="<?php echo base_url("vendor/dataTables/dataTables-config.js"); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url("vendor/dataTables/dataTables.bootstrap.min.js"); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url("vendor/dataTables/dataTables.buttons.min.js"); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url("vendor/dataTables/jszip.min.js"); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url("vendor/dataTables/pdfmake.min.js"); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url("vendor/dataTables/vfs_fonts.js"); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url("vendor/dataTables/buttons.html5.min.js"); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url("vendor/dataTables/buttons.colVis.min.js"); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url("vendor/moment/moment/min/moment.min.js"); ?>"></script>


  <script type="text/javascript" src="<?php echo base_url("vendor/moment/moment/min/moment-with-locales.min.js"); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url("vendor/components/toastr/toastr.min.js"); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url("vendor/components/toastr/toastr-config.js"); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url("vendor/bootstrap-select/bootstrap-select/dist/js/bootstrap-select.min.js"); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url("vendor/bootstrap-select/bootstrap-select/dist/js/i18n/defaults-pt_BR.js"); ?>"></script>
  <script src="<?php echo base_url("vendor/jquery-slimscroll/jquery.slimscroll.min.js");?>"></script>
  <script src="<?php echo base_url("vendor/fastclick/lib/fastclick.js");?>"></script>
  <script src="<?php echo base_url("vendor/adminLTE/js/adminlte.min.js");?>"></script>
  <!-- Conteúdo -->
  <div class="content-wrapper">
    <?php echo $conteudo ?>
  </div>
  <footer class="main-footer">
    <strong>Copyright &copy; 2017 <a href="#">Ministério da Ciência, Tecnologia, Inovações e Comunicações</a></strong>
  </footer>
  <div class="control-sidebar-bg"></div>
</div>
    </div>
  </div>
<script>
  
</script>
<script>
  function mostrarRelatorioLotacao(){
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer
    {
      $('#relatorios .loader').hide();
      window.location.href = "<?php echo base_url("relatorio/lotacao");?>";
    }
    else
    {
      window.location.href = "<?php echo base_url("relatorio/lotacao");?>";
      if(window.location.href.localeCompare("<?php echo base_url("relatorio/lotacao");?>")){
        $('#relatorios .loader').show();
      }
    }
  }

  function redirecionarParaRelatorio(idEvento){
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer
    {
      $('#relatorios .loader').hide();
      window.location.href = "<?php echo base_url("relatorio/eventosAntigos?id=") ?>" + idEvento;
    }
    else
    {
      window.location.href = "<?php echo base_url("relatorio/eventosAntigos?id=") ?>" + idEvento;
      if(window.location.href.localeCompare("<?php echo base_url("relatorio/eventosAntigos?id=") ?>" + idEvento)){
        $('#relatorios .loader').show();  
      }
    }
    // var urlParaRedirecionar = "<?php //echo base_url("relatorio/eventosAntigos?id=") ?>" + idEvento;
    // window.location.href = urlParaRedirecionar;
    //    if(window.location.href.localeCompare(urlParaRedirecionar)){
    //      $('#relatorios .loader').show();      
    //    } else {

    //    }
  }
  $(function() {
      $( "#dataInicio" ).datepicker();
      $( "#dataFim" ).datepicker();
    });

    function preencheModalComEvento(evento){
      var eventoNome = evento.getAttribute('data-evento');
      var eventoInicio = evento.getAttribute('data-inicio');
      var eventoFim = evento.getAttribute('data-fim');
      var eventoId = evento.getAttribute('data-id');

      insereDadosNaModal(eventoInicio, eventoFim, eventoNome, eventoId);
      alterarModalParaEditar();
    }

    function zerarModal(){
      document.getElementById("erros").style = "margin:20px; display:none;";
      document.getElementById("erros").innerHTML = '';

      document.getElementById("nomeEvento").value = '';
      document.getElementById("dataInicio").value = '';
      document.getElementById("dataFim").value = '';
    }
        function alterarModalParaEditar(){
      // Alterando cabeçalho
      document.getElementById("titulo_cabecalho").innerHTML = "Editar Evento";
      document.getElementById("submitEvento").innerHTML = "Editar Evento";

      // Alterando action do form
      document.getElementById("eventoForm").action = "./evento/alterarEvento";
    }

    function alterarModalParaCriar(){
      // Alterando cabeçalho
      document.getElementById("titulo_cabecalho").innerHTML = "Criar Evento";
      document.getElementById("submitEvento").innerHTML = "Criar Evento";

      // Alterando action do form
      document.getElementById("eventoForm").action = "./evento/criarEvento";
    }

    function insereDadosNaModal(eventoInicio, eventoFim, eventoNome, eventoId){
      document.getElementById("dataInicio").value = eventoInicio;
      document.getElementById("dataFim").value = eventoFim;
      document.getElementById("nomeEvento").value = eventoNome;
      document.getElementById("idEvento").value = eventoId;
    }
    function validaNomeVazio(nomeEvento){
      var textoDoErro = "- O Nome deve possuir pelo menos 1 caracter diferente de espaço. <br>";
      var nomeEventoFormatado = nomeEvento.trim();
      console.log(nomeEventoFormatado);
      if(nomeEventoFormatado != ""){
        console.log("Nome válido");
        apagarErro(textoDoErro);
        return true;
      }else{
        console.log("Nome inválido");
        escreverErro(textoDoErro);
        return false;
      }
    }

    function validaDataInicioMenorFim(dataInicial, dataFinal){
      var textoDoErro = "- A data de Início deve ser menor que a data de Término. <br>";
      var dataInicio = converterDataPadraoIngles(dataInicial);
      dataInicio = new Date(dataInicio);
      var dataFim = converterDataPadraoIngles(dataFinal);
      dataFim = new Date(dataFim);
      console.log(dataInicio + " " + dataFim);
      if(dataInicio < dataFim){
        console.log("Data válida");
        apagarErro(textoDoErro);
        return true;
      }else{
        console.log("Data inválida");
        escreverErro(textoDoErro);
        return false;
      }
    }

function validaEvento(){
      var nomeEvento = document.getElementById("nomeEvento").value;
      var dataInicio = document.getElementById("dataInicio").value;
      var dataFim = document.getElementById("dataFim").value;

      var validaDataInicioIgualMenorFim = validaDataInicioMenorFim(dataInicio, dataFim);
      var validaNomeSoComEspaco = validaNomeVazio(nomeEvento);
      var validaNomeNaoRepetido = validaNomeUnico(nomeEvento);
      var validaDataInicio = validaDataEntreEventos(dataInicio, dataFim);

      if(validaDataInicioIgualMenorFim && validaNomeSoComEspaco && validaNomeNaoRepetido && validaDataInicio){
        return true;
      }else{
        document.getElementById("erros").style = "margin:20px; display:block;";
        return false;
      }
    }


    function apagarErro(textoDoErro){
      var erros = document.getElementById("erros");
      if(erros.innerHTML.indexOf(textoDoErro) != -1){
        erros.innerHTML = erros.innerHTML.replace(textoDoErro, "");
      }
    }
    function escreverErro(textoDoErro){
      var erros = document.getElementById("erros");
      if(erros.innerHTML.indexOf(textoDoErro) == -1){
        erros.innerHTML = erros.innerHTML + textoDoErro;
      }
    }


</script>
</body>
</html>
