<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="<?php echo base_url("vendor/twbs/bootstrap/dist/css/bootstrap.min.css");?>">
  <link rel="stylesheet" href="<?php echo base_url("vendor/components/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css");?>">
  <link rel="stylesheet" href="<?php echo base_url("vendor/dataTables/dataTables.bootstrap.min.css");?>">
  <link rel="stylesheet" href="<?php echo base_url("vendor/font-awesome/css/font-awesome.min.css");?>">
  <link rel="stylesheet" href="<?php echo base_url("vendor/components/jqueryui/themes/base/jquery-ui.min.css");?>">
  <link rel="stylesheet" href="<?php echo base_url("vendor/components/toastr/toastr.min.css");?>">
  <link rel="stylesheet" href="<?php echo base_url("vendor/bootstrap-select/bootstrap-select/dist/css/bootstrap-select.min.css");?>">
  <link rel="shortcut icon" href="<?php echo base_url("img/favicon.ico");?>"/>
  <title>Sistema de Atualização Cadastral</title>
  <style>
    .navbar-inverse {
      border-radius: 0px;
    }
  </style>
</head>
<body>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo base_url("/usuario");?>">SisAC - Sistema de Atualização Cadastral</a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?=$usuario->nome_usuario?> <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo base_url("/seguranca/logout");?>"><i class="glyphicon glyphicon-log-out"></i> Sair</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>
<script type="text/javascript" src="<?php echo base_url("vendor/components/jquery/jquery.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("vendor/components/jqueryui/jquery-ui.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("vendor/components/jqueryui/ui/i18n/datepicker-pt-BR.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("vendor/components/jquerymask/jquery.inputmask.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("vendor/components/jquerymask/jquery.validate.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("vendor/twbs/bootstrap/dist/js/bootstrap.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("vendor/components/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("vendor/components/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.pt-BR.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("vendor/dataTables/jquery.dataTables.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("vendor/dataTables/dataTables.bootstrap.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("vendor/dataTables/dataTables-config.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("vendor/moment/moment/min/moment.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("vendor/moment/moment/min/moment-with-locales.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("vendor/components/toastr/toastr.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("vendor/components/toastr/toastr-config.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("vendor/bootstrap-select/bootstrap-select/dist/js/bootstrap-select.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("vendor/bootstrap-select/bootstrap-select/dist/js/i18n/defaults-pt_BR.js"); ?>"></script>
<div class="container"><?=$conteudo?></div>
</body>
</html>