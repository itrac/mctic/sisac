-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema sisac
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `sisac` ;

-- -----------------------------------------------------
-- Schema sisac
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `sisac` DEFAULT CHARACTER SET utf8 ;
USE `sisac` ;

-- -----------------------------------------------------
-- Table `sisac`.`ocupacao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sisac`.`ocupacao` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `ocupacao` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `sisac`.`cargo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sisac`.`cargo` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `cargo` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `sisac`.`perfil`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sisac`.`perfil` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `perfil` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sisac`.`empresa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sisac`.`empresa` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `empresa` VARCHAR(250) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sisac`.`setor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sisac`.`setor` (
  `id`INT NOT NULL AUTO_INCREMENT,
  `setor` VARCHAR(15) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `sisac`.`bloco`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sisac`.`bloco` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `bloco` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `sisac`.`lotacao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sisac`.`lotacao` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `lotacao` VARCHAR(255) NULL,
  `unidade` VARCHAR(255) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sisac`.`usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sisac`.`usuario` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_ad` VARCHAR(300) NULL,
  `id_ocupacao` INT NULL,
  `id_cargo` INT NULL,
  `id_perfil` INT NOT NULL,
  `id_empresa` INT NULL,
  `id_bloco` INT NULL,
  `id_setor` INT NULL,
  `id_lotacao` INT NULL,
  `nome` VARCHAR(200) NULL,
  `telefone` VARCHAR(45) NULL,
  `telefone_pessoal` VARCHAR(45) NULL,
  `email_institucional` VARCHAR(45) NULL,
  `email_pessoal` VARCHAR(45) NULL,
  `cpf` VARCHAR(45) NULL,
  `nome_usuario` VARCHAR(45) NULL,
  `sala` VARCHAR(45) NULL,
  `senha` VARCHAR(45) NULL,
  `andar` VARCHAR(45) NULL,
  `estacao` VARCHAR(15) NULL,
  `ultimo_acesso` DATETIME NULL,
  `outra_ocupacao` VARCHAR(200) NULL,
  `data_importado` DATETIME NULL,
  `data_removido` DATETIME NULL,
  `valido` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_usuario_ocupacao1_idx` (`id_ocupacao` ASC),
  INDEX `fk_usuario_cargo1_idx` (`id_cargo` ASC),
  INDEX `fk_usuario_perfil1_idx` (`id_perfil` ASC),
  INDEX `fk_usuario_empresa1_idx` (`id_empresa` ASC),
  INDEX `fk_usuario_bloco1_idx` (`id_bloco` ASC),
  INDEX `fk_usuario_coordenacao1_idx` (`id_lotacao` ASC),
  INDEX `fk_usuario_setor_idx` (`id_setor` ASC),
  CONSTRAINT `fk_usuario_ocupacao1`
    FOREIGN KEY (`id_ocupacao`)
    REFERENCES `sisac`.`ocupacao` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_cargo1`
    FOREIGN KEY (`id_cargo`)
    REFERENCES `sisac`.`cargo` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_perfil1`
    FOREIGN KEY (`id_perfil`)
    REFERENCES `sisac`.`perfil` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_empresa1`
    FOREIGN KEY (`id_empresa`)
    REFERENCES `sisac`.`empresa` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_bloco1`
    FOREIGN KEY (`id_bloco`)
    REFERENCES `sisac`.`bloco` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_coordenacao1`
    FOREIGN KEY (`id_lotacao`)
    REFERENCES `sisac`.`lotacao` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_setor`
    FOREIGN KEY (`id_setor`)
    REFERENCES `sisac`.`setor` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sisac`.`evento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sisac`.`evento` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `data_fim` DATE NULL,
  `data_inicio` DATE NULL,
  `evento` VARCHAR(255) NULL,
  `quantidade_usuarios` INT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `evento_UNIQUE` (`evento` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sisac`.`usuario_evento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sisac`.`usuario_evento` (
  `id_usuario` INT NOT NULL,
  `id_evento` INT NOT NULL,
  `data_atualizado` DATETIME NULL,
  PRIMARY KEY (`id_usuario`, `id_evento`),
  INDEX `fk_usuario_has_evento_evento1_idx` (`id_evento` ASC),
  INDEX `fk_usuario_has_evento_usuario1_idx` (`id_usuario` ASC),
  CONSTRAINT `fk_usuario_has_evento_usuario1`
    FOREIGN KEY (`id_usuario`)
    REFERENCES `sisac`.`usuario` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_has_evento_evento1`
    FOREIGN KEY (`id_evento`)
    REFERENCES `sisac`.`evento` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sisac`.`importacao_exportacao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sisac`.`importacao_exportacao` (
  `data_ultima_importacao` DATETIME NULL,
  `data_ultima_exportacao` DATETIME NULL)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `sisac`.`active_directory` (
  `host_ad` VARCHAR(255) NULL,
  `dominio_ad` VARCHAR(255) NULL,
  `usuario_administrador` VARCHAR(255) NULL,
  `senha` VARCHAR(255) NULL,
  `base_dn_ad` VARCHAR(100) NULL)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
-- begin attached script 'script'
INSERT INTO `bloco` (`id`, `bloco`) VALUES
(1, 'Bloco E'),
(2, 'Bloco R'),
(3, 'Anexo do Bloco R'),
(4, 'Cerrado Bloco F'),
(5, 'Cerrado Bloco E'),
(6, 'Cerrado Bloco B'),
(7, 'Cerrado Bloco R'),
(8, 'Cerrado Bloco S'),
(9, 'Regional - Minas Gerais (MG)'),
(10, 'Regional - São Paulo (SP)'),
(11, 'Regional - Rio de Janeiro (RJ)'),
(12, 'Regional - Goiás (GO)'),
(13, 'Regional - Rio Grande do Sul (RS)'),
(14, 'Regional - Mato Grosso (MT)'),
(15, 'Regional - Santa Catarina (SC)'),
(16, 'CEMADEN');

INSERT INTO `setor` (`id`, `setor`) VALUES
(1, 'Leste'),
(2, 'Oeste'),
(3, 'Sem Setor');

INSERT INTO `empresa` (`id`, `empresa`) VALUES
(1, 'Central IT'),
(2, 'CTIS'),
(3, 'Squadra');

INSERT INTO `evento` (`id`, `data_fim`, `data_inicio`, `evento`) VALUES
(1, '2018-09-15', '2018-03-15', 'Primeira atualização do AD.');

INSERT INTO `ocupacao` (`id`, `ocupacao`) VALUES
(1, 'Servidor'),
(2, 'Servidor Comissionado'),
(3, 'Comissionado'),
(4, 'Terceirizado'),
(5, 'Estagiário'),
(6, 'Outros');

INSERT INTO `cargo` (`id`, `cargo`) VALUES
(1, 'DAS 1'),
(2, 'DAS 2'),
(3, 'DAS 3'),
(4, 'DAS 4'),
(5, 'DAS 5'),
(6, 'DAS 6'),
(7, 'FCPE 1'),
(8, 'FCPE 2'),
(9, 'FCPE 3'),
(10, 'FCPE 4');

INSERT INTO `perfil` (`id`, `perfil`) VALUES
(1, 'Administrador'),
(2, 'Atendente da PA'),
(3, 'Usuário');

INSERT INTO `lotacao` (`unidade`,`lotacao`) VALUES
('Secretaria de Radiodifusão','SERAD'),
('Gabinete da Secretaria de Radiodifusão','SERAD/GSRAD'),
('Coordenação de Documentação e Informação','SERAD/GSRAD/CODIN'),
('Divisão de Documentação e Informação de Radiodifusão Comercial','SERAD/GSRAD/CODIN/DICOR'),
('Serviço de Documentação e Informação de Outorgas','SERAD/GSRAD/CODIN/DICOR/SEOUT'),
('Serviço de Documentação e Informação de Pós-Outorga','SERAD/GSRAD/CODIN/DICOR/SEPOS'),
('Serviço de Documentação e Informação de Televisão Digital','SERAD/GSRAD/CODIN/DICOR/SEDIG'),
('Divisão de Documentação e Informação de Radiodifusão Educativa, Comunitária e de Fiscalização','SERAD/GSRAD/CODIN/DIESF'),
('Serviço de Documentação e Informação de Radiodifusão Educativa e Consignações da União','SERAD/GSRAD/CODIN/DIESF/SERED'),
('Serviço de Documentação e Informação de Radiodifusão Comunitária','SERAD/GSRAD/CODIN/DIESF/SERCO'),
('Serviço de Documentação e Informação de Fiscalização de Outorgas','SERAD/GSRAD/CODIN/DIESF/SEFOT'),
('Divisão de Gestão da Informação','SERAD/GSRAD/CODIN/DIGIN'),
('Serviço de Gestão da Informação','SERAD/GSRAD/CODIN/DIGIN/SEGES'),
('Serviço de Cadastro de Informações de Radiodifusão','SERAD/GSRAD/CODIN/DIGIN/SECIR'),
('Serviço de Apoio Administrativo','SERAD/GSRAD/CODIN/DIGIN/SEAPA'),
('Departamento de Radiodifusão Comercial','SERAD/DECOM'),
('Divisão de Acompanhamento de Radiodifusão','SERAD/DECOM/DIARA'),
('Serviço de Acompanhamento de Radiodifusão','SERAD/DECOM/DIARA/SEARA'),
('Serviço de Controle de Ações de Radiodifusão','SERAD/DECOM/DIARA/SECAR'),
('Coordenação-Geral de Outorgas','SERAD/DECOM/CGOU'),
('Coordenação Legal dos Serviços Ancilares de Radiodifusão','SERAD/DECOM/CGOU/COLAN'),
('Divisão de Outorga Legal dos Serviços Ancilares de Radiodifusão','SERAD/DECOM/CGOU/COLAN/DILAN'),
('Serviço de Análise Legal dos Serviços Ancilares de Radiodifusão','SERAD/DECOM/CGOU/COLAN/DILAN/SELAN'),
('Serviço de Análise Legal dos Serviços de Processo Seletivo de Radiodifusão','SERAD/DECOM/CGOU/COLAN/DILAN/SELPS'),
('Coordenação Técnica dos Serviços Ancilares de Radiodifusão','SERAD/DECOM/CGOU/COTAN'),
('Divisão de Análise Técnica dos Serviços Ancilares de Radiodifusão','SERAD/DECOM/CGOU/COTAN/DITAN'),
('Serviço de Análise Técnica dos Serviços Ancilares de Radiodifusão','SERAD/DECOM/CGOU/COTAN/DITAN/SETAN'),
('Coordenação-Geral de Pós-outorgas','SERAD/DECOM/CGPO'),
('Serviço de Alterações Societárias','SERAD/DECOM/CGPO/SEASI'),
('Coordenação de Renovação de Outorga de Serviços de Radiodifusão','SERAD/DECOM/CGPO/COROR'),
('Divisão de Renovação de Outorga','SERAD/DECOM/CGPO/COROR/DIVRO'),
('Serviço Legal de Renovação de Outorga','SERAD/DECOM/CGPO/COROR/DIVRO/SELRO'),
('Serviço Técnico de Renovação de Outorga','SERAD/DECOM/CGPO/COROR/DIVRO/SETRO'),
('Coordenação de Alteração de Características Técnicas e Societárias','SERAD/DECOM/CGPO/COACT'),
('Serviço de Alteração de Características Técnicas','SERAD/DECOM/CGPO/COACT/SEACT'),
('Coordenação-Geral de Televisão Digital','SERAD/DECOM/CGTD'),
('Serviço de Apoio Técnico e Estatísticas de Televisão Digital','SERAD/DECOM/CGTD/SEETD'),
('Coordenação de Implantação da Televisão Digital','SERAD/DECOM/CGTD/COITD'),
('Divisão de Estudos e Modernização de Televisão Digital','SERAD/DECOM/CGTD/COITD/DIETD'),
('Serviço de Estudos de Televisão Digital','SERAD/DECOM/CGTD/COITD/DIETD/SESTD'),
('Serviço de Análise Técnica de Televisão Digital','SERAD/DECOM/CGTD/COITD/DIETD/SEATD'),
('Serviço de Modernização de Televisão Digital','SERAD/DECOM/CGTD/COITD/DIETD/SEMTD'),
('Coordenação de Monitoramento de Televisão Digital','SERAD/DECOM/CGTD/CORES'),
('Departamento de Radiodifusão Educativa, Comunitária e de Fiscalização','SERAD/DECEF'),
('Serviço de Acompanhamento de Radiodifusão Educativa, Comunitária e de Fiscalização','SERAD/DECEF/SEACP'),
('Serviço de Controle de Ações de Radiodifusão Educativa, Comunitária e de Fiscalização','SERAD/DECEF/SECOT'),
('Coordenação-Geral de Fiscalização de Outorgas','SERAD/DECEF/CGFI'),
('Serviço de Degravação','SERAD/DECEF/CGFI/SEDEG'),
('Coordenação de Fiscalização de Regime Legal','SERAD/DECEF/CGFI/COFIT'),
('Divisão de Fiscalização das Outorgas Legais','SERAD/DECEF/CGFI/COFIT/DIFIS'),
('Serviço de Análise de Atos Societários','SERAD/DECEF/CGFI/COFIT/DIFIS/SEATO'),
('Coordenação de Fiscalização de Conteúdo e de Aspectos não Técnicos','SERAD/DECEF/CGFI/COFIT'),
('Serviço de Análise de Infrações','SERAD/DECEF/CGFI/COFIT/SEAIC'),
('Serviço de Análise de Denúncias','SERAD/DECEF/CGFI/COFIT/SEADE'),
('Coordenação-Geral de Radiodifusão Educativa e Consignações da União','SERAD/DECEF/CGEC'),
('Coordenação do Regime Legal de Radiodifusão Educativa e Consignações da União','SERAD/DECEF/CGEC/COLEC'),
('Divisão de Outorga e Pós-Outorga de Radiodifusão Educativa e Consignações da União','SERAD/DECEF/CGEC/COLEC/DIPEC'),
('Serviço de Consignações da União e Canal da Cidadania','SERAD/DECEF/CGEC/COLEC/DIPEC/SECOC'),
('Serviço de Outorga de Radiodifusão Educativa','SERAD/DECEF/CGEC/COLEC/DIPEC/SEORE'),
('Serviço de Pós-Outorga de Radiodifusão Educativa','SERAD/DECEF/CGEC/COLEC/DIPEC/SEPRE'),
('Coordenação de Análise Técnica de Radiodifusão Educativa e Consignações da União','SERAD/DECEF/CGEC/COTED'),
('Serviço de Estudos e Análise Técnica','SERAD/DECEF/CGEC/COTED/SESTE'),
('Coordenação-Geral de Radiodifusão Comunitária','SERAD/DECEF/CGRC'),
('Serviço de Ações de Outorga','SERAD/DECEF/CGRC/SEARO'),
('Coordenação de Processos de Rádio Comunitária','SERAD/DECEF/CGRC/COPRC'),
('Divisão de Processos de Rádio Comunitária','SERAD/DECEF/CGRC/COPRC/DIPERC'),
('Serviço de Análise Pós - Jurídica','SERAD/DECEF/CGRC/COPRC/DIPERC/SEAPJ'),
('Serviço de Análise Técnica','SERAD/DECEF/CGRC/COPRC/DIPERC/SEANT'),
('Serviço de Análise de Renovação','SERAD/DECEF/CGRC/COPRC/DIPERC/SEARE'),
('Serviço de Análise de Renovação e Outorga','SERAD/DECEF/CGRC/COPRC/DIPERC/SEAOU'),
('Secretaria de Políticas e Programas de Pesquisa e Desenvolvimento','SEPED'),
('Gabinete da Secretaria da Secretaria de Políticas e Programas de Pesquisa e Desenvolvimento','SEPED/GSPED'),
('Divisão de Apoio Administrativo','SEPED/GSPED/DIADM'),
('Serviço de Apoio Administrativo','SEPED/GSPED/SEADM'),
('Coordenação-Geral de Execução e Acompanhamento de Projetos','SEPED/GSPED/CGAP'),
('Divisão de Formalização, Execução e Acompanhamento de Projetos','SEPED/GSPED/CGAP/DIEAP'),
('Departamento de Políticas e Programas de Ciências','SEPED/DEPPC'),
('Coordenação-Geral de Oceanos, Antártica e Geociências','SEPED/DEPPC/CGOA'),
('Coordenação de Mar, Antártica e Recursos Minerais','SEPED/DEPPC/CGOA/COMAR'),
('Coordenação-Geral do Clima','SEPED/DEPPC/CGCL'),
('Coordenação de Clima, Meteorologia e Climatologia','SEPED/DEPPC/CGCL/COCMC'),
('Coordenação-Geral de Ciências Humanas e Sociais Aplacadas','SEPED/DEPPC/CGHS'),
('Serviço de Acompanhamento de Projetos','SEPED/DEPPC/CGHS/SEAPR'),
('Departamento de Políticas e Programas de Desenvolvimento','SEPED/DEPPD'),
('Coordenação-Geral de Bioeconomia','SEPED/DEPPD/CGBE'),
('Coordenação de Água, Alimentos e Energia','SEPED/DEPPD/CGBE/COAAE'),
('Coordenação-Geral de Saúde e Biotecnologia','SEPED/DEPPD/CGSB'),
('Coordenação de Programas e Projetos de Saúde, Biotecnologia e Agropecuária','SEPED/DEPPD/CGSB/COSBA'),
('Coordenação-Geral de Biomas','SEPED/DEPPD/CGBI'),
('Coordenação de Biodiversidade e Ecossistemas','SEPED/DEPPD/CGBI/COBEC'),
('Departamento de Políticas e Programas para Inclusão Social','SEPED/DEPIS'),
('Coordenação-Geral de Popularização e Divulgação da Ciência','SEPED/DEPIS/CGPD'),
('Coordenação de Projetos e Espaços de Divulgação Científica','SEPED/DEPIS/CGPD/COPDC'),
('Coordenação-Geral de Extensão Tecnológica','SEPED/DEPIS/CGET'),
('Coordenação de Extensão Tecnológica','SEPED/DEPIS/CGET/COEXT'),
('Secretaria de Desenvolvimento Tecnológico e Inovação','SETEC'),
('Gabinete da Secretaria de Desenvolvimento Tecnológico e Inovação','SETEC/GSTEC'),
('Serviço de Apoio Administrativo','SETEC/GSTEC/SEAAD'),
('Serviço de Apoio Técnico e Orçamentário','SETEC/GSTEC/SEATO'),
('Departamento de Políticas e Programas de Apoio à Inovação','SETEC/DEPAI'),
('Coordenação-Geral de Ambientes Inovadores e Empreendedorismo','SETEC/DEPAI/CGIE'),
('Coordenação de Ambientes Inovadores','SETEC/DEPAI/CGIE/COAMB'),
('Coordenação de Empreendedorismo','SETEC/DEPAI/CGIE/COEMP'),
('Coordenação-Geral de Incentivos ao Desenvolvimento Tecnológico e Inovação','SETEC/DEPAI/CGIT'),
('Coordenação de Incentivos e Transferência de Tecnologia','SETEC/DEPAI/CGIT/COITT'),
('Coordenação-Geral de Serviços Tecnológicos','SETEC/DEPAI/CGST'),
('Coordenação de Serviços Tecnológicos e Gestão da Inovação','SETEC/DEPAI/CGST/COSGI'),
('Departamento de Políticas de Desenvolvimento e Inovação de Tecnologias Estruturantes','SETEC/DETEC'),
('Coordenação-Geral de Desenvolvimento e Inovação em Tecnologias Setoriais','SETEC/DETEC/CGTS'),
('Coordenação de Inovação em Tecnologias Setoriais','SETEC/DETEC/CGTS/COITS'),
('Coordenação-Geral de Desenvolvimento e Inovação em Tecnologias Convergentes e Habilitadoras','SETEC/DETEC/CGTC'),
('Coordenação de Inovação em Tecnologias Convergentes e Habilitadoras','SETEC/DETEC/CGTC/COITC'),
('Coordenação-Geral de Desenvolvimento e Inovação em Tecnologias Estratégicas','SETEC/DETEC/CGTE'),
('Coordenação de Inovação em Tecnologias Estratégicas','SETEC/DETEC/CGTE/COITE'),
('Secretaria de Telecomunicações','SETEL'),
('Gabinete da Secretaria de Telecomunicações','SETEL/GSTEL'),
('Departamento de Serviços de Telecomunicações','SETEL/DETEL'),
('Departamento de Banda Larga','SETEL/DEBAN'),
('Coordenação de Programas de Infraestrutura de Banda Larga','SETEL/DEBAN/COINB'),
('Departamento de Inclusão Digital','SETEL/DEIDI'),
('Coordenação-Geral de Formação, Sistemas e Infraestrutura','SETEL/DEIDI/CGIN'),
('Coordenação de Formação','SETEL/DEIDI/CGIN/COFOR'),
('Coordenação de Infraestrutura','SETEL/DEIDI/CGIN/COINF'),
('Coordenação de Sistemas','SETEL/DEIDI/CGIN/COSIS'),
('Coordenação-Geral de Articulação','SETEL/DEIDI/CGAT'),
('Coordenação de Articulação','SETEL/DEIDI/CGAT/COART'),
('Coordenação Gestão Administrativa e Financeira','SETEL/DEIDI/CGAT/COADF'),
('Secretaria de Politicas Digitais','SEPOD'),
('Gabinete da Secretaria de Política de Informática','SEPOD/GSPIN'),
('Divisão de Acompanhamento Administrativo-Financeiro','SEPOD/GSPIN/DIAFI'),
('Departamento de Políticas e Programas Setoriais em Tecnologia da Informação e Comunicações','SEPOD/DETIC'),
('Coordenação-Geral de Assuntos Cibernéticos','SEPOD/DETIC/CGAC'),
('Coordenação-Geral de Agenda Digital','SEPOD/DETIC/CGAD'),
('Departamento de Ecossistemas Digitais','SEPOD/DECOD'),
('Coordenação-Geral de Plataformas e Software','SEPOD/DECOD/CGSP'),
('Divisão de Plataformas Digitais','SEPOD/DECOD/CGSP/DIPLD'),
('Coordenação-Geral de Ambiente de Negócios','SEPOD/DECOD/CGAN'),
('Departamento de Ciência, Tecnologia e Inovação Digital','SEPOD/DETIC'),
('Coordenação-Geral de Incentivo à Inovação Digital','SEPOD/DETIC/CGID'),
('Coordenação de Fomento à Inovação','SEPOD/DETIC/CGID/COFIN'),
('Divisão de Pesquisa, Desenvolvimento e Inovação','SEPOD/DETIC/CGID/DIPDI'),
('Coordenação-Geral de Ciência e Tecnologia','SEPOD/DETIC/CGCT'),
('Divisão de Acompanhamento e Avaliação','SEPOD/DETIC/CGCT/DIAAV'),
('Coordenação de Inovação Industrial','SEPOD/DETIC/CGCT/COIIN'),
('Coordenação de Microeletrônica','SEPOD/DETIC/CGCT/COMCE'),
('Assessoria Especial de Controle Interno','AECI'),
('Assessoria Especial de Assuntos Internacionais','ASSIN'),
('Serviço de Gestão de Documentos','ASSIN/SEGED'),
('Coordenação de Cooperação Internacional Multilateral','ASSIN/COCUM'),
('Divisão de Foros Multilaterais e Iniciativas Temáticas','ASSIN/COCUM/DIFIT'),
('Divisão de Organismos Internacionais','ASSIN/COCUM/DIORI'),
('Coordenação de Cooperação Internacional Bilateral','ASSIN/COINB'),
('Divisão de Cooperação com Europa e América do Sul e Central','ASSIN/COINB/DICEA'),
('Divisão de Cooperação com América do Norte, Ásia, África e Oceania','ASSIN/COINB/DICAO'),
('Divisão de Apoio Administrativo','ASSIN/COINB/DIAAD'),
('Coordenação-Geral de Bens Sensíveis','ASSIN/CGBS'),
('Coordenação de Implementação, Acompanhamento e Controle nas áreas Nuclear, Química, Biológica e de Mísseis','ASSIN/CGBS/COCBS'),
('Subsecretaria de Conselhos e Comissões','SGCC'),
('Coordenação do Conselho Nacional de Ciência e Tecnologia','SGCC/COCCT'),
('Coordenação da Comissão Técnica Nacional de Biossegurança','SGCC/CTNBio'),
('Coordenação da Secretaria-Executiva do Conselho Nacional de Controle de Experimentação Animal','SGCC/CONCEA'),
('Escritório Regional de São Paulo','ERESP'),
('Gabinete do Ministro','GM'),
('Coordenação-Geral do Gabinete do Ministro','GM/CGGM'),
('Divisão de Gestão do Gabinete do Ministro','GM/CGGM/DIGGM'),
('Divisão de Acompanhamento e Expediente','GM/CGGM/DIEXP'),
('Ouvidoria','GM/OUVID'),
('Cerimonial','GM/CERIM'),
('Assessoria de Comunicação Social','GM/ASCOM'),
('Coordenação Administrativa de Imprensa','GM/ASCOM/COIMP'),
('Coordenação Administrativa de Publicidade','GM/ASCOM/COPUB'),
('Assessoria de Assuntos Parlamentares','GM/ASPAR'),
('Coordenação de Acompanhamento do Processo Legislativo e Análise de Informações','GM/ASPAR/COPLE'),
('Divisão de Análise de Informações','GM/ASPAR/DIAAI'),
('Secretaria Executiva','SEXEC'),
('Gabinete da Secretaria-Executiva','SEXEC/GABEX'),
('Divisão de Apoio ao Gabinete','SEXEC/GABEX/DIGAB'),
('Divisão de Documentação e Arquivo','SEXEC/GABEX/DIDOC'),
('Corregedoria','SEXEC/CORREG'),
('Diretoria de Gestão das Unidades de Pesquisa e Organizações Sociais','SEXEC/DPO'),
('Divisão de Apoio Administrativo','SEXEC/DPO/DIAMI'),
('Coordenação-Geral de Unidades de Pesquisa e Organizações Sociais','SEXEC/DPO/CGUO'),
('Coordenação das Unidades de Pesquisa','SEXEC/DPO/CGUO/COUPE'),
('Coordenação de Organizações Sociais','SEXEC/DPO/CGUO/COORS'),
('Coordenação de Avaliação','SEXEC/DPO/CGUO/COAVL'),
('Diretoria de Gestão de Entidades Vinculadas','SEXEC/DGV'),
('Coordenação-Geral de Governança e Acompanhamento de Entidades Vinculadas','SEXEC/DGV/CGEV'),
('Coordenação de Governança das Entidades Vinculadas','SEXEC/DGV/CGEV/COGEV'),
('Coordenação de Serviços Postais','SEXEC/DGV/CGEV/COSEP'),
('Diretoria de Gestão Estratégica - DGE','SEXEC/DGE'),
('Coordenação-Geral de Planejamento Estratégico e Setorial','SEXEC/DGE/CGPE'),
('Coordenação de Planejamento e Avaliação','SEXEC/DGE/CGPE/COPLA'),
('Coordenação de Projetos Especiais','SEXEC/DGE/CGPE/COPES'),
('Coordenação-Geral de Gestão, Inovação e Indicadores','SEXEC/DGE/CGGI'),
('Coordenação de Indicadores e Informação','SEXEC/DGE/CGGI/COIND'),
('Serviço de Arquivo e Biblioteca','SEXEC/DGE/CGGI/COIND/SEARB'),
('Coordenação de Organização Institucional','SEXEC/DGE/CGGI/COORG'),
('Coordenação de Desenvolvimento de Pessoas','SEXEC/DGE/CGGI/CODEP'),
('Divisão de Desenvolvimento de Pessoas','SEXEC/DGE/CGGI/CODEP/DIDEP'),
('Serviço de Avaliações de Desempenho','SEXEC/DGE/CGGI/CODEP/DIDEP/SERAV'),
('Coordenação-Geral de Governança de Fundos','SEXEC/DGE/CGGF'),
('Serviço de Apoio aos Colegiados','SEXEC/DGE/CGGF/SEACO'),
('Coordenação de Planejamento e Gestão de Fundos','SEXEC/DGE/CGGF/COGEF'),
('Coordenação Técnica e Operacional dos Fundos','SEXEC/DGE/CGGF/COTEF'),
('Diretoria de Administração','SEXEC/DAD'),
('Divisão de Apoio à Diretoria de Administração','SEXEC/DAD/DIVAD'),
('Coordenação-Geral de Orçamento e Finanças','SEXEC/DAD/CGOF'),
('Coordenação de Orçamento','SEXEC/DAD/CGOF/COORC'),
('Divisão de Programação Orçamentária','SEXEC/DAD/CGOF/COORC/DIPOR'),
('Serviço de Avaliação e Elaboração da Programação Orçamentária','SEXEC/DAD/CGOF/COORC/DIPOR/SEAEX'),
('Divisão de Acompanhamento da Execução da Programação Orçamentária','SEXEC/DAD/CGOF/COORC/DIEPO'),
('Serviço de Acompanhamento da Execução da Programação Orçamentária','SEXEC/DAD/CGOF/COORC/DIEPO/SEAOR'),
('Coordenação Financeira','SEXEC/DAD/CGOF/CORFI'),
('Divisão de Programação Financeira','SEXEC/DAD/CGOF/CORFI/DIPFI'),
('Serviço de Programação e Liberação Financeira','SEXEC/DAD/CGOF/CORFI/DIPFI/SEPLF'),
('Coordenação de Contabilidade','SEXEC/DAD/CGOF/COTAB'),
('Divisão de Análise Contábil','SEXEC/DAD/CGOF/COTAB/DIACO'),
('Serviço de Acompanhamento Contábil','SEXEC/DAD/CGOF/COTAB/DIACO/SEACC'),
('Divisão de Informação de Custos','SEXEC/DAD/CGOF/COTAB/DIINC'),
('Serviço de Apuração de Custos','SEXEC/DAD/CGOF/COTAB/DIINC/SERAC'),
('Coordenação-Geral de Gestão de Pessoas','SEXEC/DAD/CGGP'),
('Coordenação de Administração de Pessoal','SEXEC/DAD/CGGP/COAPE'),
('Divisão de Cadastro de Pessoal','SEXEC/DAD/CGGP/COAPE/DICAD'),
('Serviço de Acompanhamento de Movimentação de Pessoal','SEXEC/DAD/CGGP/COAPE/DICAD/SEAMP'),
('Divisão de Benefícios','SEXEC/DAD/CGGP/COAPE/DIBEN'),
('Serviço de Atenção à Saúde do Servidor','SEXEC/DAD/CGGP/COAPE/DIBEN/SEASS'),
('Coordenação de Pagamento, Execução Orçamentária e Financeira de Pessoal','SEXEC/DAD/CGGP/COPEO'),
('Divisão de Pagamento de Pessoal','SEXEC/DAD/CGGP/COPEO/DIPAG'),
('Serviço de Pagamento de Pessoal','SEXEC/DAD/CGGP/COPEO/DIPAG/SEPAG'),
('Divisão de Execução Orçamentária e Financeira','SEXEC/DAD/CGGP/COPEO/DIOFI'),
('Coordenação de Informações e Legislação de Pessoal','SEXEC/DAD/CGGP/COLEG'),
('Divisão de Assuntos Judiciais de Pessoal','SEXEC/DAD/CGGP/COLEG/DILEG'),
('Serviço de Controle de Assuntos Jurídicos de Pessoal','SEXEC/DAD/CGGP/COLEG/DILEG/SECAJ'),
('Divisão de Informações e Normas de Pessoal','SEXEC/DAD/CGGP/COLEG/DIINF'),
('Coordenação de Aposentarias e Pensões','SEXEC/DAD/CGGP/COAPP'),
('Divisão de Aposentadorias e Pensões','SEXEC/DAD/CGGP/COAPP/DIPEN'),
('Serviço de Análise e Concessão de Aposentadoria','SEXEC/DAD/CGGP/COAPP/DIPEN/SEAPO'),
('Serviço de Concessão de Pensões','SEXEC/DAD/CGGP/COAPP/DIPEN/SEPEN'),
('Serviço de Revisão de Pensões','SEXEC/DAD/CGGP/COAPP/DIPEN/SERPE'),
('Coordenação-Geral de Recursos Logísticos','SEXEC/DAD/CGRL'),
('Coordenação de Licitações, Compras e Contratos','SEXEC/DAD/CGRL/COLCC'),
('Divisão de Compras e Instrução Processual','SEXEC/DAD/CGRL/COLCC/DICIP'),
('Serviço de Compras e Cadastro','SEXEC/DAD/CGRL/COLCC/DICIP/SECOC'),
('Serviço de Instrução Processual','SEXEC/DAD/CGRL/COLCC/DICIP/SEINP'),
('Serviço de Licitações','SEXEC/DAD/CGRL/COLCC/DICIP/SELIC'),
('Divisão de Licitações e Contratos','SEXEC/DAD/CGRL/COLCC/DILIC'),
('Serviço de Atos e Acompanhamento de Contratos','SEXEC/DAD/CGRL/COLCC/DILIC/SEAAC'),
('Coordenação de Infraestrutura Predial','SEXEC/DAD/CGRL/COINT'),
('Divisão de Obras e Engenharia','SEXEC/DAD/CGRL/COINT/DIOBE'),
('Serviço de Administração de Edifícios','SEXEC/DAD/CGRL/COINT/DIOBE/SEADI'),
('Coordenação de Logística e Patrimônio','SEXEC/DAD/CGRL/COLOP'),
('Divisão de Serviços Gerais','SEXEC/DAD/CGRL/COLOP/DISEG'),
('Serviço de Atividades Auxiliares','SEXEC/DAD/CGRL/COLOP/DISEG/SEATA'),
('Serviço de Transportes','SEXEC/DAD/CGRL/COLOP/DISEG/SETRA'),
('Serviço de Apoio Avançado','SEXEC/DAD/CGRL/COLOP/DISEG/SEAAV'),
('Serviço de Protocolo-Geral','SEXEC/DAD/CGRL/COLOP/DISEG/SEPRG'),
('Divisão de Material e Patrimônio','SEXEC/DAD/CGRL/COLOP/DIMAP'),
('Serviço de Almoxarifado e Patrimônio','SEXEC/DAD/CGRL/COLOP/DIMAP/SEALP'),
('Coordenação de Execução Orçamentária e Financeira','SEXEC/DAD/CGRL/COEOF'),
('Divisão de Execução Orçamentária','SEXEC/DAD/CGRL/COEOF/DIEXO'),
('Serviço de Controle Orçamentário','SEXEC/DAD/CGRL/COEOF/DIEXO/SECOO'),
('Divisão de Análise e Execução Financeira','SEXEC/DAD/CGRL/COEOF/DIAEF'),
('Serviço de Pagamentos e Prestação de Contas','SEXEC/DAD/CGRL/COEOF/DIAEF/SEPPC'),
('Diretoria de Tecnologia da Informação','SEXEC/DTI'),
('Coordenação-Geral de Governança de Tecnologia da Informação','SEXEC/DTI/CGGO'),
('Coordenação de Planos e Normas de Tecnologia da Informação','SEXEC/DTI/CGGO/COPLN'),
('Divisão de Projetos e Processos','SEXEC/DTI/CGGO/COPLN/DIPRO'),
('Divisão de Aquisições de Tecnologia da Informação','SEXEC/DTI/CGGO/COPLN/DIATI'),
('Coordenação-Geral de Sistemas','SEXEC/DTI/CGSI'),
('Coordenação de Projetos de Sistemas','SEXEC/DTI/CGSI/COPRO'),
('Divisão de Desenvolvimento e Manutenção','SEXEC/DTI/CGSI/COPRO/DIDEM'),
('Coordenação de Qualidade de Software','SEXEC/DTI/CGSI/COSOF'),
('Divisão de Administração de Dados','SEXEC/DTI/CGSI/COSOF/DIADD'),
('Coordenação-Geral de Serviços de Tecnologia da Informação','SEXEC/DTI/CGTI'),
('Serviço de Atendimento ao Usuário','SEXEC/DTI/CGTI/SEAUS'),
('Serviço de Telefonia','SEXEC/DTI/CGTI/SERTE'),
('Coordenação de Redes e Segurança Cibernética','SEXEC/DTI/CGTI/COSEG'),
('Divisão de Segurança de Rede e Banco de Dados','SEXEC/DTI/CGTI/COSEG/DISEB'),
('Consultoria Jurídica','CONJUR'),
('Gabinete do Consultor Jurídico','CONJUR/Gabinete do Consultor Jurídico'),
('Coordenação de Organização Administrativa','CONJUR/Gabinete do Consultor Jurídico/COADM'),
('Divisão de Documentação Jurídica','CONJUR/Gabinete do Consultor Jurídico/DIJUR'),
('Divisão de Apoio Administrativo','CONJUR/Gabinete do Consultor Jurídico/DIAPA'),
('Coordenação-Geral de Assuntos Jurídicos de Comunicação','CONJUR/CGJC'),
('Coordenação Jurídica de Radiodifusão Comercial e de Serviços Ancilares','CONJUR/CGJC/CORSA'),
('Coordenação Jurídica de Licitação de Radiodifusão','CONJUR/CGJC/COLIR'),
('Coordenação Jurídica de Radiodifusão Educativa e Comunitária','CONJUR/CGJC/COREC'),
('Coordenação-Geral de Licitações, Contratos e Atos Correlatos','CONJUR/CGLC'),
('Coordenação Jurídica de Licitação e Contratos','CONJUR/CGLC/COLIC'),
('Coordenação Jurídica de Convênios e Atos Correlatos','CONJUR/CGLC/COCAC'),
('Coordenação-Geral de Assuntos Judiciais','CONJUR/CGAJ'),
('Coordenação Jurídica de Contencioso Judicial','CONJUR/CGAJ/COJUD'),
('Coordenação-Geral de Assuntos Jurídicos de Ciência, Tecnologia e Inovações','CONJUR/CGCI'),
('Coordenação Jurídica de Assuntos Científicos','CONJUR/CGCI/COACI'),
('Coordenação Jurídica de Tecnologia e Inovações','CONJUR/CGCI/COTEC'),
('Coordenação-Geral de Telecomunicações, Supervisão e Pessoal','CONJUR/CGTP'),
('Coordenação Jurídica de Supervisão e Pessoal','CONJUR/CGTP/COSUP');

INSERT INTO `usuario` (`id`, `id_ad`, `id_ocupacao`, `id_perfil`, `id_setor`, `id_cargo`, `id_empresa`, `id_bloco`, `id_lotacao`, `nome`, `telefone`, `telefone_pessoal`, `email_institucional`, `email_pessoal`, `cpf`, `nome_usuario`, `sala`, `senha`, `andar`, `ultimo_acesso`,`valido`) VALUES
(1, null, null, 1, null, null, null, null, null, 'Administrador','', '', 'admin@mctic.gov.br', 'admin@gmail.com', '123.456.789-99', 'admin', 'Sala 1', '827ccb0eea8a706c4c34a16891f84e7b', 'Térreo', '2018-03-15', 1),
(2, null, null, 3, null, null, null, null, null, 'Usuário DB','', '3027-9999', 'usuario@mctic.gov.br', 'usuario@gmail.com', '123.456.789-99', 'usuario.db', 'Sala 2', '827ccb0eea8a706c4c34a16891f84e7b', 'Térreo', '2018-03-15', 1);
-- end attached script 'script'

