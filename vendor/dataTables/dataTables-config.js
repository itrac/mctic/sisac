var dataTableConfig = {
    "language": {
        "url": "./vendor/dataTables/Portuguese-Brasil.json"
    },

	dom: 'Bfrtip',
        buttons: [
            {
                extend: 'colvis', text: 'Escolher Colunas',   
            },
            {
                extend: 'excelHtml5', text: 'Exportar para Excel',
                exportOptions: {
                    columns: [0, 1, 2, 3]
                }
            },
            {
                extend: 'pdfHtml5', text: 'Exportar para PDF',
                exportOptions: {
                    columns: [ 0, 1, 2, 3 ]
                }
            }
        ]    	
};

